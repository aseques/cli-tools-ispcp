<?php

/**
 * Outputs the debug message if debug is enabled
 *
 * @param string $text message to output
*/
function debug($text) {
    //Retrieve system config
    $cfg = iMSCP_Registry::get('config');
    if ($cfg->DEBUG==1)
        if (!is_array($text)) {
            echo "DEBUG: ".$text."\n";
        } else {
            print_r($text);
        }
}

/**
 * Outputs the info message if debug is enabled
 *
 * @param string $text message to output
*/
function info($text) {
    //Retrieve system config
    $cfg = iMSCP_Registry::get('config');
    if ($cfg->DEBUG==1)
        echo "INFO: ".$text."\n";
}


/**
 * Outputs the an error formatted message
 *
 * @param string $text message to output
*/
function error($text) {
    //Retrieve system config
    $cfg = iMSCP_Registry::get('config');
    echo "ERROR: ".$text."\n";
}

/**
 * Validate the language code (form en_UK)
 *
 * @param string $text message to output
*/
function validate_lang($text) {
    $pattern = '/^[a-z]{2}_[A-Z]{2}/';
    return preg_match($pattern, $text);
}


/**
 * Waits until the rqst-mngr is not active
 *
 *
*/


function wait_rqst() {
    $rqst_on = 2;
    while ($rqst_on!='1') {
    unset($output);
    exec("pgrep -xf '/usr/bin/perl /var/www/imscp/engine/imscp-rqst-mngr'", $output, $rqst_on);
    foreach($output as $oi=>$o) if(strpos($o,'pgrep')!==false) unset($output[$oi]);
    $rqst_on = !count($output);
    if ($rqst_on != 0) {
        debug("rqst-mngr not running, we can continue");
    } else {
        info("rqst-mngr is already running, waiting ..");
        sleep(2);
    }
    }
}

//This file includes some functions taken out of domain.php to
//	clarify and modularize the code

/**
 * Reads line from the socket resource.
 *
 * @param resource &$socket
 * @return string A line read from the socket resource
 */

if (!function_exists('read_line')) {

    function read_line(&$socket)
    {
        $line = '';

        do {
            $ch = socket_read($socket, 1);
            $line = $line . $ch;
        } while ($ch != "\r" && $ch != "\n");

        return $line;
    }
}

/**
 * Send a request to the daemon.
 *
 * @return string Daemon answer
 * @todo Remove error operator
 */
if (!function_exists('send_request')) {
function send_request()
{
    /** @var $cfg  iMSCP_Config_Handler_File */
    $cfg = iMSCP_Registry::get('config');

    //$code = 999;

    @$socket = socket_create(AF_INET, SOCK_STREAM, 0);
    if ($socket < 0) {
        $errno = "socket_create() failed.\n";
        return $errno;
    }

    @$result = socket_connect($socket, '127.0.0.1', 9876);
    if ($result == false) {
        $errno = "socket_connect() failed.\n";
        return $errno;
    }

    // read one line with welcome string
    $out = read_line($socket);

    list($code) = explode(' ', $out);
    if ($code == 999) {
        return $out;
    }

    // send hello query
    $query = "helo  {$cfg->Version}\r\n";
    socket_write($socket, $query, strlen($query));

    // read one line with helo answer
    $out = read_line($socket);

    list($code) = explode(' ', $out);
    if ($code == 999) {
        return $out;
    }

    // send reg check query
    $query = "execute query\r\n";
    socket_write($socket, $query, strlen($query));
    // read one line key replay
    $execute_reply = read_line($socket);

    list($code) = explode(' ', $execute_reply);
    if ($code == 999) {
        return $out;
    }

    // send quit query
    $quit_query = "bye\r\n";
    socket_write($socket, $quit_query, strlen($quit_query));

    // read quit answer
    $quit_reply = read_line($socket);

    list($code) = explode(' ', $quit_reply);

    if ($code == 999) {
        return $out;
    }

    list($answer) = explode(' ', $execute_reply);

    socket_close($socket);

    return $answer;
}
}


/**
 * Replaces special encoded strings back to their original signs
 *
 * @author Benedikt Heintel <benedikt.heintel@ispcp.net>
 * @param string $string String to replace chars
 * @return String with replaced chars
 */
function replace_html($string)
{
    $pattern = array(
        '#&lt;[ ]*b[ ]*&gt;#i', '#&lt;[ ]*/[ ]*b[ ]*&gt;#i',
        '#&lt;[ ]*strong[ ]*&gt;#i', '#&lt;[ ]*/[ ]*strong[ ]*&gt;#i',
        '#&lt;[ ]*em[ ]*&gt;#i', '#&lt;[ ]*/[ ]*em[ ]*&gt;#i',
        '#&lt;[ ]*i[ ]*&gt;#i', '#&lt;[ ]*/[ ]*i[ ]*&gt;#i',
        '#&lt;[ ]*small[ ]*&gt;#i', '#&lt;[ ]*/[ ]*small[ ]*&gt;#i',
        '#&lt;[ ]*br[ ]*(/|)[ ]*&gt;#i');

    $replacement = array(
        '<b>', '</b>', '<strong>', '</strong>', '<em>', '</em>', '<i>', '</i>',
        '<small>', '</small>', '<br />');

    $string = preg_replace($pattern, $replacement, $string);

    return $string;
}



/************************************************************************************
 * Logging related functions
 */

/**
 * Writes a log message in the database and sends it to the administrator by email according log level.
 *
 * @param string $msg Message to log
 * @param int $logLevel Log level Loggin level from which log is sent via mail
 * @return void
 */
function write_log($sql,$msg, $logLevel = E_USER_WARNING)
{
    /** @var $cfg iMSCP_Config_Handler_File */
    $cfg = iMSCP_Registry::get('config');

    $clientIp = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : 'unknown';

// 	$msg = replace_html($msg . '<br /><small>User IP: ' . $clientIp . '</small>', ENT_COMPAT, tr('encoding'));
    $msg = replace_html($msg . '<br /><small>User IP: ' . $clientIp . '</small>');

    $query = "INSERT INTO `log` (`log_time`,`log_message`) VALUES(NOW(), ?)";
// 	exec_query($query, $msg);
    $stmt=$sql->prepare($query);
    if(!$stmt->execute(array($msg))) {
        echo "ERROR: Couldn't insert the log information";
    }

    $msg = strip_tags(str_replace('<br />', "\n", $msg));
    $to = isset($cfg->DEFAULT_ADMIN_ADDRESS) ? $cfg->DEFAULT_ADMIN_ADDRESS : '';

    if ($to != '' && $logLevel <= $cfg->LOG_LEVEL) {
        $hostname = isset($cfg->SERVER_HOSTNAME) ? $cfg->SERVER_HOSTNAME : '';
        $baseServerIp = isset($cfg->BASE_SERVER_IP) ? $cfg->BASE_SERVER_IP : '';
        $version = isset($cfg->Version) ? $cfg->Version : 'unknown';
        $buildDate = isset($cfg->BuildDate) ? $cfg->BuildDate : 'unknown';
        $subject = "i-MSCP $version on $hostname ($baseServerIp)";

        if ($logLevel == E_USER_NOTICE) {
            $severity = 'Notice (You can ignore this message)';
        } elseif ($logLevel == E_USER_WARNING) {
            $severity = 'Warning';
        } elseif ($logLevel == E_USER_ERROR) {
            $severity = 'Error';
        } else {
            $severity = 'Unknown';
        }

        $message = <<<AUTO_LOG_MSG

i-MSCP Log

Server: $hostname ($baseServerIp)
Version: i-MSCP $version ($buildDate)
Message severity: $severity

Message: ----------------[BEGIN]--------------------------

$msg

Message: ----------------[END]----------------------------

_________________________
i-MSCP Log Mailer

Note: If you want no longer receive this kind of message,
you can change the logging level via the settings page.

AUTO_LOG_MSG;

        $headers = "From: \"i-MSCP Logging Mailer\" <" . $to . ">\n";
        $headers .= "MIME-Version: 1.0\nContent-Type: text/plain; charset=utf-8\n";
        $headers .= "Content-Transfer-Encoding: 7bit\n";
        $headers .= "X-Mailer: i-MSCP $version Logging Mailer";

        if (!mail($to, $subject, $message, $headers)) {
            $log_message =
                "Logging Mailer Mail To: |$to|, From: |$to|, Status: |NOT OK|!";

            $query = "INSERT INTO `log` (`log_time`,`log_message`) VALUES(NOW(), ?)";
            exec_query($query, $log_message, false);
        }
    }
}


/**
 * Adds the 3 mail accounts/forwardings to a new domain....
 * @param PDOObject $sql Handler for the sql server
 * @param int $dmn_id
 * @param string $user_email
 * @param string $dmn_part
 * @param string $dmn_type
 * @param int $sub_id
 * @return void
 */
function client_mail_add_default_accounts($sql, $dmn_id, $user_email, $dmn_part,
    $dmn_type = 'domain', $sub_id = 0)
{
    /** @var $cfg iMSCP_Config_Handler_File */
    $cfg = iMSCP_Registry::get('config');

    if ($cfg->CREATE_DEFAULT_EMAIL_ADDRESSES) {
        $forward_type = ($dmn_type == 'alias') ? 'alias_forward' : 'normal_forward';

        // prepare SQL
        $query = "
            INSERT INTO
                mail_users (
                    `mail_acc`, `mail_pass`, `mail_forward`,`domain_id`, `mail_type`,
                    `sub_id`, `status`, `mail_auto_respond`,`quota`, `mail_addr`
                ) VALUES (
                    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
                )
        ";

    $stmt=$sql->prepare($query);
        // create default forwarder for webmaster@domain.tld to the account's owner
        $stmt->execute(array('webmaster', '_no_', $user_email, $dmn_id,
                                $forward_type, $sub_id, $cfg->ITEM_TOADD_STATUS,
                                '_no_', 10485760, 'webmaster@' . $dmn_part));

        // create default forwarder for postmaster@domain.tld to the account's reseller
        $stmt->execute(array('postmaster', '_no_', $_SESSION['user_email'],
                                $dmn_id, $forward_type, $sub_id,
                                $cfg->ITEM_TOADD_STATUS, '_no_', 10485760,
                                'postmaster@' . $dmn_part));

        // create default forwarder for abuse@domain.tld to the account's reseller
        $stmt->execute(array('abuse', '_no_', $_SESSION['user_email'],
                                $dmn_id, $forward_type, $sub_id,
                                $cfg->ITEM_TOADD_STATUS, '_no_', 10485760,
                                'abuse@' . $dmn_part));
    }
}


/**
 * Deletes a SQL user.
 *
 * Note: Please, be sure to execute this function inside a MYSQL transaction to ensure data consistency.
 *
 * @param  int $domainId Domain unique identifier
 * @param  int $sqlUserId Sql user unique identifier
 * @return bool TRUE if $sqlUserId has been found and successfully removed, FALSE otherwise
 */
function sql_delete_user($sql, $domainId, $sqlUserId)
{
    iMSCP_Events_Manager::getInstance()->dispatch(iMSCP_Events::onBeforeDeleteSqlUser, array('sqlUserId' => $sqlUserId));

    $query = "
        SELECT
            `t1`.`sqld_id`, `t1`.`sqlu_name`, `t2`.`sqld_name`, `t1`.`sqlu_name`
        FROM
            `sql_user` `t1`, `sql_database` `t2`
        WHERE
            `t1`.`sqld_id` = `t2`.`sqld_id`
        AND
            `t2`.`domain_id` = ?
        AND
            `t1`.`sqlu_id` = ?
    ";
    $stmt=$sql->prepare($query);
    $stmt->execute(array($domainId, $sqlUserId));

    $values=$stmt->fetchAll();
    if (count($values)=='0') {
        return false;
    }

    $value=array_pop($values);
    $sqlUserName = $value['sqlu_name'];

    // If SQL user is only assigned to one database we can remove it completely
// print_r($value);
    if (count_sql_user_by_name($sql,$value['sqlu_name']) == 1) {
        $query = 'DELETE FROM `mysql`.`user` WHERE `User` = ?';
        $stmt=$sql->prepare($query);
        $stmt->execute(array($sqlUserName));

        $query = 'DELETE FROM `mysql`.`db` WHERE `User` = ?';
        $stmt=$sql->prepare($query);
        $stmt->execute(array($sqlUserName));

    } else {
        $query = 'DELETE FROM `mysql`.`db` WHERE `User` = ? AND `Db` = ?';
        $stmt=$sql->prepare($query);
        $stmt->execute(array($sqlUserName, $value['sqld_name']));
    }

    // Flush mysql privileges
    $query = "FLUSH PRIVILEGES";
    $sql->query($query);

    // Delete the database from the i-MSCP sql_user table
    // Must be done at end of process
    $query = 'DELETE FROM `sql_user` WHERE `sqlu_id` = ?';
    $stmt=$sql->prepare($query);
    $stmt->execute(array($sqlUserId));

    // Update reseller sql user limit
    //NOT necessary when deleting a domain
// 	update_reseller_c_props(get_reseller_id($domainId));

    iMSCP_Events_Manager::getInstance()->dispatch(iMSCP_Events::onAfterDeleteSqlUser, array('sqlUserId' => $sqlUserId));

    return true;
}


/**
 * Deletes a SQL database.
 *
 * @param  int $domainId Domain unique identifier
 * @param  int $databaseId Databse unique identifier
 * @return bool TRUE when $databaseId has been found and successfully deleted, FALSE otherwise
 */
function delete_sql_database($sql, $domainId, $databaseId)
{
    iMSCP_Events_Manager::getInstance()->dispatch(iMSCP_Events::onBeforeDeleteSqlDb, array('sqlDbId' => $databaseId));

    // Get name of $databaseId being deleted
    $query = "SELECT `sqld_name` FROM `sql_database` WHERE `domain_id` = ? AND `sqld_id` = ?";
echo $query."\n";
// print_r(array($domainId, $databaseId));
// 	$stmt = exec_query($query, array($domainId, $databaseId));
    $stmt=$sql->prepare($query);
    $stmt->execute(array($domainId, $databaseId));
    $values=$stmt->fetchAll();
    if (count($values)==0) {
        return false;
    }
// print_r($values);
// print_r(count($values));
// return false;
// 	$databaseName = quoteIdentifier($stmt->fields['sqld_name']);
    $databaseNametmp=array_pop($values);
    $databaseName = $databaseNametmp['sqld_name'];
// 	$databaseName = $sql->quote($databaseNametmp['sqld_name']);
// print_r($databaseName);
// exit(0);

    // Get list of users assigned to $databaseId
    $query = "
        SELECT
            `t2`.`sqlu_id`
        FROM
            `sql_database` `t1`, `sql_user` `t2`
        WHERE
            `t1`.`sqld_id` = `t2`.`sqld_id`
        AND
            `t1`.`domain_id` = ?
        AND
            `t1`.`sqld_id` = ?
    ";
// 	$stmt = exec_query($query, array($domainId, $databaseId));
    $stmt=$sql->prepare($query);
    $stmt->execute(array($domainId, $databaseId));

    $values=$stmt->fetchAll();
    if (count($values)>0) {
        while ($value=array_pop($values)) {
            $sqlUserId = $value['sqlu_id'];
            if (!sql_delete_user($sql, $domainId, $sqlUserId)) {
                throw new iMSCP_Exception(sprintf('Unable to delete SQL user linked to database with ID %d.', $sqlUserId));
            }
        }
    }

    $query = "DELETE FROM `sql_database` WHERE `domain_id` = ? AND `sqld_id` = ?";
    $stmt=$sql->prepare($query);
    $stmt->execute(array($domainId, $databaseId));

    //Not needed when doing a domain deletion
    //update_reseller_c_props(get_reseller_id($databaseId));

    // Must be done last du to the implicit commit
    $sql->query("DROP DATABASE IF EXISTS $databaseName");

    iMSCP_Events_Manager::getInstance()->dispatch(iMSCP_Events::onAfterDeleteSqlDb, array('sqlDbId' => $databaseId));

    return true;
}


/**
 * Count SQL user by name.
 *
 * @param string $sqlu_name SQL user name to match against
 * @return int
 */
function count_sql_user_by_name($sql, $sqlu_name)
{
    $query = "
        SELECT
            COUNT(*) AS `cnt`
        FROM
            `sql_user`
        WHERE
            `sqlu_name` = ?
    ";
    $stmt=$sql->prepare($query);
    $stmt->execute(array($sqlu_name));
    $value=$stmt->fetch();
    return $value['cnt'];
}
