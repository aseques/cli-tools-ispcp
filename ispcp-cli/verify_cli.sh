#!/bin/sh

#Program help
help () {
  echo "$0 -a (actions to do: create/delete/all)"
  echo "  -d (domain: defaults to example.com)"
  echo "  -p (password: defaults to pas123)"
  exit 0
}

while getopts ":ha:d:p:" opt; do
  case $opt in
    a)
      ACTION=$OPTARG;
      ;;
    d)
      DOMAIN=$OPTARG;
      ;;
    p)
      PASSWORD=$OPTARG;
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      help
      exit 1
      ;;
    h)
      help
      ;;
    :)
      echo "The option -$OPTARG needs a parameter" >&2
      help
      exit 1
      ;;
  esac
done

if [ -d "/etc/imscp" ] ; then
	printf "##We are on a imscp system##\n"
	CLIDOM=domain_imscp.php
	CLIDNS=domain_imscp_dns.php
else
	CLIDOM=domain.php
	CLIDNS=domain_dns.php
fi

if [ ! -e "/usr/bin/realpath" ] ; then
        echo "#WARN, the realpath utility could not be found, please install it"
        echo "#apt-get install realpath"
        exit 0
else
        REAL=`realpath $0`
        BASE=`dirname $REAL`
fi

if [ "$ACTION" != "create" -a "$ACTION" != "delete" ] ; then
	ACTION='all'
fi

if [ -z "$DOMAIN" ] ; then
	DOMAIN=example.com
fi

if [ -z "$PASSWD" ] ; then
        PASSWD=pas123
fi

LOG=/tmp/.verify.log

if [ "$ACTION" = "all" -o "$ACTION" = "create" ] ; then
	printf "\n#At first, we create the domain, and all of its attributes\n" >>$LOG
	#Domain
	echo "php $BASE/$CLIDOM --action create_domain --domain $DOMAIN --password $PASSWD" >>$LOG
	#Needs timewait before creating the ftp user
	printf "while [ ! -d /var/www/virtual/$DOMAIN/htdocs/ ]; do sleep 1; done\n" >>$LOG
	#FTP
	echo "php $BASE/$CLIDOM --action create_ftp --domain $DOMAIN --password $PASSWD --user ftp" >>$LOG
	#Default mailboxes
	echo "php $BASE/$CLIDOM --action create_default_mail --domain $DOMAIN" >>$LOG
	#Mail account
	echo "php $BASE/$CLIDOM --action create_mail --domain $DOMAIN  --password $PASSWD --user mailbox" >>$LOG
	#Mail alias
	echo "php $BASE/$CLIDOM --action create_mail_alias --domain $DOMAIN --mail_dst webmaster@$DOMAIN --user temp" >>$LOG
	#Domain alias
	echo "php $BASE/$CLIDOM --action create_dom_alias --domain prova2.com --dom_dst $DOMAIN" >>$LOG
	#Subdomain
	echo "php $BASE/$CLIDOM --action create_subdomain --domain $DOMAIN --subdomain test" >>$LOG
	#Database
	echo "php $BASE/$CLIDOM --action create_db --domain $DOMAIN --db_name exampledb" >>$LOG
	#Database user
	echo "php $BASE/$CLIDOM --action create_db_user --domain $DOMAIN --db_name exampledb --password $PASSWD --user exampleuser" >>$LOG
	#View stats
	echo "php $BASE/$CLIDOM --action 'create_htaccess_user' --domain $DOMAIN --user 'userstats' --password $PASSWD;" >>$LOG
	echo "php $BASE/$CLIDOM --action 'join_htaccess_group' --domain $DOMAIN --user 'userstats';" >>$LOG
	#Fixing permission issues
	echo "php $BASE/$CLIDOM --action 'fix_htdocs_owner' --domain $DOMAIN;" >>$LOG
	#Changing the domain access password
	echo "php $BASE/$CLIDOM --action 'change_domain_pass' --domain $DOMAIN --password $PASSWD;" >>$LOG
        #Set the mail limit to a different value for the plan
        echo php $BASE/$CLIDOM --action 'set_mail_limit' --domain $DOMAIN --limit --1 >>$LOG

	#DNS entries
	echo "php $BASE/$CLIDNS --action create_dns --domain $DOMAIN --dns_type MX --dns_txt mx1.$DOMAIN. --dns_prio 10" >>$LOG
        echo "php $BASE/$CLIDNS --action create_dns --domain $DOMAIN --dns_type TXT --dns_txt \"v=spf1 mx a:mail.$DOMAIN ~all\"" >>$LOG
	#Prior deleting the domain, we try to regenerate it (rebuild the configurations)
	#echo php $BASE/$CLIDOM --action 'regenerate_domain' --domain $DOMAIN >>$LOG
fi

if [ "$ACTION" = "all" -o "$ACTION" = "delete" ] ; then
        echo "php $BASE/$CLIDNS --action delete_dns --domain $DOMAIN --dns_type MX --dns_txt mx1.$DOMAIN. --dns_prio 10" >>$LOG
        echo "php $BASE/$CLIDNS --action delete_dns --domain $DOMAIN --dns_type TXT --dns_txt \"v=spf1 mx a:mail.$DOMAIN ~all\"" >>$LOG
	#Finally, we delete the account once verified that everything is working
	# as expected
	printf "\n#Deleting the domain and aliases...\n" >>$LOG
	echo php $BASE/$CLIDOM --action delete_alias --domain prova2.com --dom_dst $DOMAIN >>$LOG
	echo php $BASE/$CLIDOM --action delete_domain --domain $DOMAIN >>$LOG
fi

cat $LOG
rm $LOG
