<?php

// Define library path
if (is_dir('/var/www/imscp/gui/library')) {
	define('LIBRARY_PATH', '/var/www/imscp/gui/library');
} else {
	throw new Exception('imscp library directory not found on your system');
}

// Set include path
set_include_path(DEFAULT_INCLUDE_PATH . PATH_SEPARATOR . LIBRARY_PATH);

// Autoloader for iMSCP class
spl_autoload_register(
	function($classname)
	{
		$path = str_replace('_', '/', $classname);

		if (file_exists(LIBRARY_PATH . '/' . $path . '.php')) {
			require_once LIBRARY_PATH . '/' . $path . '.php';
		}
	}
);

/**
 * Dummy class that allows to bypass the i-MSCP bootstrap process
 */
class iMSCP_Bootstrap { public static function boot() { } }

/**
 * Dummy class that allows to bypass the i-MSCP initialization process
 */
class iMSCP_Initializer { public static function run() { } }

// Load configuration
require_once LIBRARY_PATH . '/environment.php';

// Load config from imscp.conf file
$config = iMSCP_Config::getInstance();
iMSCP_Registry::set('config', $config);

// Load config parameters from database (also make database connection in same time)
$dbConfig = new iMSCP_Config_Handler_Db(dbConnect());

// Merge db config with file config
$config->replaceWith($dbConfig);

// Remove useless variables
unset($config, $dbConfig);

/***********************************************************************
/* Bootstrap Routines
 */

/**
 * Returns clear text database password.
 *
 * @param string $dbPasword
 * @return string
 */
function decryptDbPassword($dbPasword)
{
	if ($dbPasword == '') {
		return '';
	}

	if (extension_loaded('mcrypt')) {
		$text = @base64_decode($dbPasword . "\n");
		$td = @mcrypt_module_open('blowfish', '', 'cbc', '');
		$key = iMSCP_Registry::get('MCRYPT_KEY');
		$iv = iMSCP_Registry::get('MCRYPT_IV');

		// Initialize encryption
		@mcrypt_generic_init($td, $key, $iv);

		// Decrypt encrypted string
		$decrypted = @mdecrypt_generic($td, $text);
		@mcrypt_module_close($td);

		return trim($decrypted);
	} else {
		throw new Exception("PHP extension 'mcrypt' not loaded");
	}
}

/**
 * Returns PDO instance.
 *
 * @return PDO
 * @throws Exception
 */
function dbConnect()
{
	static $connection = null;

	if (null === $connection) {
		$cfg = iMSCP_Registry::get('config');

		$db_pass_key = $db_pass_iv = '';

		eval(@file_get_contents($cfg->CONF_DIR . '/imscp-db-keys'));

		if (!empty($db_pass_key) && !empty($db_pass_iv)) {
			iMSCP_Registry::set('MCRYPT_KEY', $db_pass_key);
			iMSCP_Registry::set('MCRYPT_IV', $db_pass_iv);
		} else {
			throw new Exception('Database key and/or initialization vector was not generated');
		}

		try {
			$connection = new PDO(
				$cfg->DATABASE_TYPE . ':host=' . $cfg->DATABASE_HOST . ';dbname=' . $cfg->DATABASE_NAME,
				$cfg->DATABASE_USER,
				decryptDbPassword($cfg->DATABASE_PASSWORD)
			);

			$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			throw new Exception('Unable to establish the connection to the database. SQL returned: ' . $e->getMessage());
		}

		if (isset($cfg->DATABASE_UTF8) && $cfg->DATABASE_UTF8 == 'yes') {
			try {
				$connection->query('SET NAMES `utf8`');
			} catch (PDOException $e) {
				throw new Exception(
					'Error: Unable to set charset for database communication. SQL returned: ' . $e->getMessage()
				);
			}
		}

		// Register PDO instance in registry for further usage.
		iMSCP_Registry::set('db', $connection);
	}

	return $connection;
}

/**
 * Convert string to IDNA ASCII.
 *
 * @param $string
 * @return string
 */
function encode_idna($string)
{
	return idn_to_ascii($string);
}
