#!/usr/bin/php -q
<?php
/**
 *
 * @copyright 	2009 by jjuvan@grn.cat
 * @version 	SVN: $ID$
 * @author 		jjuvan@grn.cat
 *
 * @license
 *   This program is licensed under GPL. See COPYING for details
 */

ini_set('display_errors', E_ALL);
ini_set('display_errors', 1);

require_once 'bootstrap.php';
require_once 'config.php';
require_once 'getopts.php';
require_once 'common_imscp.php';

$opts = getopts(array(
    'action' => array('switch' => 'action', 'type' => GETOPT_VAL),
    'domain' => array('switch' => 'domain', 'type' => GETOPT_VAL),
    'dns_type' => array('switch' => 'dns_type', 'type' => GETOPT_VAL),
    'dns_sub' => array('switch' => 'dns_sub', 'type' => GETOPT_VAL),
    'dns_prio' => array('switch' => 'dns_prio', 'type' => GETOPT_VAL),
    'dns_owner' => array('switch' => 'dns_owner', 'type' => GETOPT_VAL),
    'dns_txt' => array('switch' => 'dns_txt', 'type' => GETOPT_VAL)
),$_SERVER['argv']);

//Check for values
//TODO, check for --help flag and explain better the create_ftp/create_domain
if (empty($opts['action'])) {
        echo "Action required (--action) and one of create_dns, delete_dns\n";
        exit (0);
} else {
    $action=$opts['action'];
}

//Required fields depends on actions, one of
//	domain, password, admin_type, created_by, email, user, dns_owner ...
$domain = trim($opts['domain']);
$dns_type = trim($opts['dns_type']);
$dns_txt = trim($opts['dns_txt']);
$dns_prio = trim($opts['dns_prio']);


if (empty($opts['domain'])) {
    echo "Domain name required, will be the same as de admin user" .
        " for that domain  (--domain example.com)\n";
    exit (0);
} elseif (empty($opts['dns_type'])) {
        echo "DNS type (--dns_type) (A,CNAME,MX,AAAA,TXT ...) is required\n";
        exit (0);
} elseif((empty($opts['dns_sub'])) && (($opts['dns_type']!="MX") && ($opts['dns_type']!="TXT"))) {
        echo "Subdomain name is required (--dns_sub 'www')\n";
        exit (0);
} elseif (empty($opts['dns_txt'])) {
        echo "We need to know the CNAME or IP this dns entry points to (--dns_txt a.example.com. or --dns_txt 123.123.123.123)\n";
        exit (0);
}
//Check for custom_dns
if ($action=='create_dns') {
        if((empty($opts['dns_owner'])) || ($opts['dns_owner']=='no') || ($opts['dns_owner']=='custom_dns_feature')) {
                $dns_owner='custom_dns_feature';
        } else {
                $dns_owner=$opts['dns_prot'];
        }
}

//Double check to ensure that empty IS empty
$dns_sub = $opts['dns_sub'];
if (empty($opts['dns_sub'])) {
    $dns_sub = "";
}

//When creating a dns type TXT, dns_sub has to be the domain.
if ($dns_type == "TXT") {
    $dns_sub = $domain.".";
    $dns_txt = "\"".$dns_txt."\"";
}

//In case we have the dns_prio flag set and the dns_type == "MX" we
//	change the dns_txt to the concatenation of both
if ($dns_type == "MX") {
    if (($dns_type == "MX") && !empty($dns_prio)) {
        $dns_txt = $dns_prio . "\t" . $dns_txt;
        //The name field (dns_sub) must be the domain finished with a '.'
        $dns_sub = $domain.".";
    } else {
        echo "ERROR: MX creation requires the priority --dns_prio number\n";
        exit(0);
    }
}

//DB Connection
$mysqli = new mysqli($imscpdb_host, $imscpdb_user, $imscpdb_password, $imscpdb_name);

if (mysqli_connect_errno()) {
    printf("ERROR: Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//We can't start while rqst-mngr is running
wait_rqst();


switch ($action) {
    case "create_dns":
        //Requires $domain, $dns_type, $dns_sub, $dns_txt
        //Optional $dns_owner (default is custom_dns_feature)
        info("Creating DNS entry $dns_sub for $domain");
        unset($domain_id);
        $qDomain="SELECT domain_id FROM domain WHERE domain_name LIKE '$domain'";
        $rDomain= $mysqli->query($qDomain);
        if($rDomain->num_rows>0){
            //es un domini
            $aDomain=$rDomain->fetch_array();
            $domain_id=$aDomain['domain_id'];
            $alias_id='0';
        }else{
            $qAlias="SELECT alias_id, domain_id FROM domain_aliasses WHERE alias_name LIKE '$domain'";
            $rAlias=$mysqli->query($qAlias);
            if($rAlias->num_rows>0){
                //es un alias de domini
                $aAlias=$rAlias->fetch_array();
                $domain_id=$aAlias['domain_id'];
                $alias_id=$aAlias['alias_id'];
            }else{
                error("You are trying to use a non existant domain");
                exit(0);
            }
        }
        if(isset($domain_id)) {
            if($alias_id!='0'){
                $qExist = "SELECT * FROM domain_dns WHERE domain_id LIKE '$domain_id' ".
                    "AND alias_id LIKE '$alias_id' AND domain_dns LIKE '$dns_sub' ".
                    "AND domain_type LIKE '$dns_type' AND domain_text LIKE '$dns_txt'";
            }else{
                $qExist = "SELECT * FROM domain_dns WHERE domain_id LIKE '$domain_id' ".
                    "AND domain_dns LIKE '$dns_sub' AND alias_id='0' ".
                    "AND domain_type LIKE '".$dns_type."' AND domain_text LIKE '$dns_txt'";
            }
            $rExist=$mysqli->query($qExist);
            if($rExist->num_rows>0){
                error("DNS entry already exists on the database");
            }else{
                $qSave = "INSERT INTO domain_dns(domain_dns_id, domain_id, alias_id, domain_dns, ".
                    "domain_class, domain_type, domain_text, owned_by) ".
                    "VALUES('','$domain_id',$alias_id,'$dns_sub','IN','$dns_type','$dns_txt','$dns_owner');";
                if(!$mysqli->query($qSave)){
                    error("while adding the DNS subdomain got: ".$mysqli->error);
                    exit(0);
                }

                $qSave = "UPDATE `domain` SET `domain`.`domain_status` = 'tochange' " .
                    "WHERE `domain`.`domain_id` = " . $domain_id;
                if (!$mysqli->query($qSave)){
                    error("While adding the DNS subdomain got: ".$mysqli->error);
                    exit(0);
                }

                $qSave = "UPDATE `subdomain` SET `subdomain`.`subdomain_status` = 'tochange' " .
                    "WHERE `subdomain`.`domain_id` = " . $domain_id;
                if (!$mysqli->query($qSave)){
                    printf("ERROR: There was an error while changing the DNS subdomain: %s\n", $mysqli->error);
                    exit(0);
                }

                if($alias_id!='0'){
                    $qSave="UPDATE domain_aliasses SET domain_aliasses.alias_status = 'tochange' " .
                        "WHERE `domain_aliasses`.`alias_id` = " .$alias_id;
                    if (!$mysqli->query($qSave)){
                        printf("ERROR There was an error while changing the DNS subdomain: %s\n", $mysqli->error);
                        exit(0);
                    }
                }
                info("DNS entry successfully added");
            }
        }
    break;
    case "delete_dns":
        //Requires $domain, $dns_type, $dns_sub, $dns_txt
        info("Deleting DNS entry $dns_sub type $dns_type for $domain pointing to $dns_txt");

        unset($domain_id);
        $qDomain="SELECT domain_id FROM domain WHERE domain_name LIKE '$domain'";
        $rDomain= $mysqli->query($qDomain);
        if($rDomain->num_rows>0){
            //es un domini
            $aDomain=$rDomain->fetch_array();
            $domain_id=$aDomain['domain_id'];
            $alias_id='0';
        }else{
            $qAlias="SELECT alias_id, domain_id FROM domain_aliasses WHERE alias_name LIKE '$domain'";
            $rAlias=$mysqli->query($qAlias);
            if($rAlias->num_rows>0){
                //es un alias de domini
                $aAlias=$rAlias->fetch_array();
                $domain_id=$aAlias['domain_id'];
                $alias_id=$aAlias['alias_id'];
            }else{
                error("You are trying to use a non existant domain");
                exit(0);
            }
        }
        if(isset($domain_id)) {
            if($alias_id!='0'){
                $qExist = "SELECT * FROM domain_dns WHERE domain_id LIKE '$domain_id' ".
                    "AND alias_id LIKE '$alias_id' AND domain_dns LIKE '$dns_sub' ".
                    "AND domain_type LIKE '$dns_type'";
            }else{
                $qExist = "SELECT * FROM domain_dns WHERE domain_id LIKE '$domain_id' ".
                    "AND domain_dns LIKE '$dns_sub' AND alias_id='0' ".
                    "AND domain_type LIKE '".$dns_type."'";
            }
            $rExist=$mysqli->query($qExist);
            if($rExist->num_rows>0) {
                $aExist=$rExist->fetch_array();
                $domain_dns_id=$aExist['domain_dns_id'];
                $qdel="DELETE FROM domain_dns WHERE domain_dns_id LIKE '".$domain_dns_id."';";
                $rdel= $mysqli->query($qdel);
                $qSave = "UPDATE `subdomain` SET `subdomain`.`subdomain_status` = 'tochange' " .
                    "WHERE `subdomain`.`domain_id` = " . $domain_id;
                if (!$mysqli->query($qSave)){
                    printf("ERROR: There was an error while changing the DNS subdomain: %s\n", $mysqli->error);
                    exit(0);
                }
                if($alias_id!='0'){
                    $qSave="UPDATE domain_aliasses SET domain_aliasses.alias_status = 'tochange' " .
                        "WHERE `domain_aliasses`.`alias_id` = " .$alias_id;
                    if (!$mysqli->query($qSave)){
                        printf("ERROR There was an error while changing the DNS subdomain: %s\n", $mysqli->error);
                        exit(0);
                    }
                }
            } else {
                error("ERROR: The dns you are trying to delete does not exist");
            }
        }
    break;
    default:
        echo "ERROR: You have selected a non existant action\n";
        exit(0);
    break;
}

//If we could reach this point, then we tell the engine to complete
//	the process
send_request();

?>
