#!/usr/bin/php -q
<?php
/**
 *
 * @copyright 	2009 by jjuvan@grn.cat
 * @version 	SVN: $ID$
 * @author 		jjuvan@grn.cat
 *
 * @license
 *   This program is licensed under GPL. See COPYING for details
 */

include '/var/www/ispcp/gui/include/ispcp-lib.php';

ini_set('display_errors', E_ALL);
ini_set('display_errors', 1);

require_once 'config.php';
require_once 'getopts.php';

$opts = getopts(array(
	'action' => array('switch' => 'action', 'type' => GETOPT_VAL),
	'domain' => array('switch' => 'domain', 'type' => GETOPT_VAL), 
	'dns_type' => array('switch' => 'dns_type', 'type' => GETOPT_VAL), 
	'dns_sub' => array('switch' => 'dns_sub', 'type' => GETOPT_VAL), 
	'dns_prio' => array('switch' => 'dns_prio', 'type' => GETOPT_VAL), 
	'dns_prot' => array('switch' => 'dns_prot', 'type' => GETOPT_VAL),
	'dns_txt' => array('switch' => 'dns_txt', 'type' => GETOPT_VAL)
),$_SERVER['argv']);

//Check for values
//TODO, check for --help flag and explain better the create_ftp/create_domain
if (empty($opts['action'])) {
        echo "Action required (--action) and one of create_dns, delete_dns\n";
        exit (0);
} else {
	$action=$opts['action'];
}

//Required fields depends on actions, one of
//	domain, password, admin_type, created_by, email, user, dns_prot ...
$domain = trim($opts['domain']);
$dns_type = trim($opts['dns_type']);
$dns_txt = trim($opts['dns_txt']);
$dns_prio = trim($opts['dns_prio']);


if (empty($opts['domain'])) {
	echo "Domain name required, will be the same as de admin user" .
		" for that domain  (--domain example.com)\n";
	exit (0);
} elseif (empty($opts['dns_type'])) {
        echo "DNS type (--dns_type) (A,CNAME,MX,AAAA,TXT ...) is required\n";
        exit (0);
} elseif((empty($opts['dns_sub'])) && (($opts['dns_type']!="MX") && ($opts['dns_type']!="TXT"))) {
        echo "Subdomain name is required (--dns_sub 'www')\n";
        exit (0);
} elseif (empty($opts['dns_txt'])) {
        echo "We need to know the CNAME or IP this dns entry points to (--dns_txt a.example.com. or --dns_txt 123.123.123.123)\n";
        exit (0);
}
//Check for dns_protected
if ($action=='create_dns') {
        if((empty($opts['dns_prot'])) || (($opts['dns_prot']!='yes') && ($opts['dns_prot']!='no'))) {
                echo "WARNING: You should set dns_prot to allow or deny users to edit dns\n";
                $dns_prot='no';
        } else {
                $dns_prot=$opts['dns_prot'];
        }
}

//Double check to ensure that empty IS empty
$dns_sub = $opts['dns_sub'];
if (empty($opts['dns_sub'])) { 
	$dns_sub = "";
}

//In case we have the dns_prio flag set and the dns_type == "MX" we 
//	change the dns_txt to the concatenation of both
if ($dns_type == "MX") {
	if (($dns_type == "MX") && !empty($dns_prio)) {
		$dns_txt = $dns_prio . "\t" . $dns_txt;
		//The name field (dns_sub) must be the domain finished with a '.'
		$dns_sub = $domain.".";
	} else {
		echo "ERROR: MX creation requires the priority --dns_prio number\n";
		exit(0);
	}
}

//DB Connection
$mysqli = new mysqli($imscpdb_host, $imscpdb_user, $imscpdb_password, $imscpdb_name);

if (mysqli_connect_errno()) {
	printf("ERROR: Connect failed: %s\n", mysqli_connect_error());
	exit();
}

switch ($action) {
	case "create_dns":
		//Requires $domain, $dns_type, $dns_sub, $dns_txt
		//Optional $dns_prot (default is no)
 		echo "Creating DNS entry $dns_sub for $domain \n";

		unset($domain_id);
		$qDomain="SELECT domain_id FROM domain WHERE domain_name LIKE '$domain'";
		$rDomain= $mysqli->query($qDomain);
		if($rDomain->num_rows>0){
			//es un domini
			$aDomain=$rDomain->fetch_array();
			$domain_id=$aDomain['domain_id'];
			$alias_id='0';
		}else{
			$qAlias="SELECT alias_id, domain_id FROM domain_aliasses WHERE alias_name LIKE '$domain'";
			$rAlias=$mysqli->query($qAlias);
			if($rAlias->num_rows>0){
				//es un alias de domini
				$aAlias=$rAlias->fetch_array();
				$domain_id=$aAlias['domain_id'];
				$alias_id=$aAlias['alias_id'];
			}else{
				echo "ERROR: You are trying to use a non existant domain\n";
				exit(0);
			}
		}
		if(isset($domain_id)) {
			if($alias_id!='0'){
				$qExist = "SELECT * FROM domain_dns WHERE domain_id LIKE '$domain_id' ".
					"AND alias_id LIKE '$alias_id' AND domain_dns LIKE '$dns_sub' ".
					"AND domain_type LIKE '$dns_type' AND domain_text LIKE '$dns_txt'";
			}else{
				$qExist = "SELECT * FROM domain_dns WHERE domain_id LIKE '$domain_id' ".
					"AND domain_dns LIKE '$dns_sub' AND alias_id='0' ".
					"AND domain_type LIKE '".$dns_type."' AND domain_text LIKE '$dns_txt'";
			}
			$rExist=$mysqli->query($qExist);
			if($rExist->num_rows>0){
				echo "ERROR: The dns entry already exists on the database\n";
			}else{
				//patch for SPF
				if ($dns_type=='TXT')
				{
				$dns_txt = "\"" . $dns_txt . "\"";
				}
				$qSave = "INSERT INTO domain_dns(domain_dns_id, domain_id, alias_id, domain_dns, ".
					"domain_class, domain_type, domain_text, protected) ".
					"VALUES('','$domain_id',$alias_id,'$dns_sub','IN','$dns_type','$dns_txt','$dns_prot');";
				if(!$mysqli->query($qSave)){
					echo "There was an error while adding the DNS subdomain\n";
					printf("ERROR: %s\n", $mysqli->error);
					exit(0);
				}
				
				$qSave = "UPDATE `domain` SET `domain`.`domain_status` = 'change' " .
					"WHERE `domain`.`domain_id` = " . $domain_id;
				if (!$mysqli->query($qSave)){
					printf("ERROR There was an error while adding the DNS subdomain: %s\n", $mysqli->error);
					exit(0);
				}

				$qSave = "UPDATE `subdomain` SET `subdomain`.`subdomain_status` = 'change' " . 
					"WHERE `subdomain`.`domain_id` = " . $domain_id;
				if (!$mysqli->query($qSave)){
					printf("ERROR: There was an error while changing the DNS subdomain: %s\n", $mysqli->error);
					exit(0);
				}
				
				if($alias_id!='0'){
					$qSave="UPDATE domain_aliasses SET domain_aliasses.alias_status = 'change' " .
						"WHERE `domain_aliasses`.`alias_id` = " .$alias_id;
					if (!$mysqli->query($qSave)){
						printf("ERROR There was an error while changing the DNS subdomain: %s\n", $mysqli->error);
						exit(0);
					}
				}
				echo "DNS entry successfully added\n";
			}
		}
	break;
	case "delete_dns":
		//Requires $domain, $dns_type, $dns_sub, $dns_txt
		echo "Deleting DNS entry $dns_sub for $domain \n";

		unset($domain_id);
		$qDomain="SELECT domain_id FROM domain WHERE domain_name LIKE '$domain'";
		$rDomain= $mysqli->query($qDomain);
		if($rDomain->num_rows>0){
			//es un domini
			$aDomain=$rDomain->fetch_array();
			$domain_id=$aDomain['domain_id'];
			$alias_id='0';
		}else{
			$qAlias="SELECT alias_id, domain_id FROM domain_aliasses WHERE alias_name LIKE '$domain'";
			$rAlias=$mysqli->query($qAlias);
			if($rAlias->num_rows>0){
				//es un alias de domini
				$aAlias=$rAlias->fetch_array();
				$domain_id=$aAlias['domain_id'];
				$alias_id=$aAlias['alias_id'];
			}else{
				echo "ERROR: You are trying to use a non existant domain\n";
				exit(0);
			}
		}
		if(isset($domain_id)) {
			if($alias_id!='0'){
				$qExist = "SELECT * FROM domain_dns WHERE domain_id LIKE '$domain_id' ".
					"AND alias_id LIKE '$alias_id' AND domain_dns LIKE '$dns_sub' ".
					"AND domain_type LIKE '$dns_type' AND domain_text LIKE '$dns_txt'";
			}else{
				$qExist = "SELECT * FROM domain_dns WHERE domain_id LIKE '$domain_id' ".
					"AND domain_dns LIKE '$dns_sub' AND alias_id='0' ".
					"AND domain_type LIKE '".$dns_type."' AND domain_text LIKE '$dns_txt'";
			}
			$rExist=$mysqli->query($qExist);
			if($rExist->num_rows>0) {
				$aExist=$rExist->fetch_array();
				$domain_dns_id=$aExist['domain_dns_id'];
				$qdel="DELETE FROM domain_dns WHERE domain_dns_id LIKE '".$domain_dns_id."';";
				$rdel= $mysqli->query($qdel);
				$qSave = "UPDATE `subdomain` SET `subdomain`.`subdomain_status` = 'change' " . 
					"WHERE `subdomain`.`domain_id` = " . $domain_id;
				if (!$mysqli->query($qSave)){
					printf("ERROR: There was an error while changing the DNS subdomain: %s\n", $mysqli->error);
					exit(0);
				}
				if($alias_id!='0'){
					$qSave="UPDATE domain_aliasses SET domain_aliasses.alias_status = 'change' " .
						"WHERE `domain_aliasses`.`alias_id` = " .$alias_id;
					if (!$mysqli->query($qSave)){
						printf("ERROR There was an error while changing the DNS subdomain: %s\n", $mysqli->error);
						exit(0);
					}
				}
			} else {
				echo "ERROR: The dns you are trying to delete does not exist\n";
			}
		}
	break;
	default:
		echo "ERROR: You have selected a non existant action\n";
		exit(0);
	break;
}
			
//If we could reach this point, then we tell the engine to complete 
//	the process
send_request();
	
?>
