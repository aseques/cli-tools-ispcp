#!/usr/bin/php -q
<?php
/**
 *
 * @copyright 	2009 by jjuvan@grn.cat
 * @version 	SVN: $ID$
 * @author 		jjuvan@grn.cat
 *
 * @license
 *   This program is licensed under GPL. See COPYING for details
 */

include '/var/www/ispcp/gui/include/ispcp-lib.php';

ini_set('display_errors', E_ALL);
ini_set('display_errors', 1);

set_include_path('/root/carpeta_desti/scripts/');
require_once 'config.php';
require_once 'getopts.php';
require_once 'common.php';

$opts = getopts(array(
	'action' => array('switch' => 'action', 'type' => GETOPT_VAL),
	'domain' => array('switch' => 'domain', 'type' => GETOPT_VAL), 
	'password' => array('switch' => 'password', 'type' => GETOPT_VAL),
	'user_lang' => array('switch' => 'user_lang', 'type' => GETOPT_VAL, 'default' => $user_lang),
	'created_by' => array('switch' => 'created_by', 'type' => GETOPT_VAL, 'default' => $created_by),
	'mail' => array('switch' => 'mail', 'type' => GETOPT_VAL, 'default' => $default_mail),
	'domain_ip_id' => array('switch' => 'domain_ip_id', 'type' => GETOPT_VAL, 'default' => $default_ip_id),
	'hosting_plan' => array('switch' => 'hosting_plan', 'type' => GETOPT_VAL, 'default' => $default_hosting_plan),
	'plan_owner' => array('switch' => 'plan_owner', 'type' => GETOPT_VAL, 'default' => $plan_owner),
	'user' => array('switch' => 'user', 'type' => GETOPT_VAL),
	'dom_dst' => array('switch' => 'dom_dst', 'type' => GETOPT_VAL),
	'db_name' => array('switch' => 'db_name', 'type' => GETOPT_VAL),
	'subdomain' => array('switch' => 'subdomain', 'type' => GETOPT_VAL),
	'mail_dst' => array('switch' => 'mail_dst', 'type' => GETOPT_VAL),
	'limit' => array('switch' => 'limit', 'type' => GETOPT_VAL)
),$_SERVER['argv']);

//Check for values
//TODO, check for --help flag and explain better the create_ftp/create_domain
if (empty($opts['action'])) {
        echo "Action required (--action) and one of: \n\tcreate_domain\n\tcreate_ftp " . 
		"\n\tcreate_default_mail \n\tcreate_mail \n\tfix_htdocs_owner \n\tcreate_mail_alias " .
		"\n\tcreate_dom_alias \n\tcreate_subdomain \n\tcreate_db \n\tcreate_db_user " .
		"\n\tregenerate_domain \n\tdelete_domain \n\tdelete_alias ".
		"\n\tcreate_htaccess_user \n\tjoin_htaccess_group\n\tchange_domain_pass\n\tset_mail_limit\n";
        exit (0);
} else {
	$action=$opts['action'];
}

if (empty($opts['domain'])) {
	echo "Domain name required, will be the same as de admin user" .
		" for that domain  (--domain example.com)\n";
	exit (0);
} elseif (empty($opts['password']) && ($action != 'fix_htdocs_owner') && ($action != 'create_dom_alias')
 		&& ($action != 'create_mail_alias') && ($action != 'create_db') && ($action !=  'create_subdomain') && ($action != 'regenerate_domain') && ($action != 'create_default_mail') && ($action != 'delete_domain') && ($action != 'delete_alias') && ($action != 'join_htaccess_group') && ($action != 'set_mail_limit')) {
	echo "Password required (--password)\n";
	exit (0);
} elseif ((empty($opts['created_by'])  && ($action == 'create_domain'))) {
        echo "Created_by is required (--created_by)\n";
        exit (0);
} elseif (empty($opts['mail'])) {
        echo "Email is required\n";
        exit (0);
} elseif (empty($opts['domain_ip_id'])) {
        echo "Domain ip id required\n";
        exit (0);
} elseif (empty($opts['hosting_plan'])) {
        echo "Hosting plan is required (--hosting_plan)\n";
        exit (0);
} elseif (empty($opts['plan_owner'])) {
        echo "Plan owner (admin or reseller) is required (--plan_owner)\n";
        exit (0);
} elseif (empty($opts['user']) && (($action == 'create_mail') || ($action == 'create_db_user') || ($action == 'join_htaccess_group') || ($action == 'create_htaccess_user') || ($action == 'create_mail_alias'))) {
        echo "The user is required (--user)\n";
        exit (0);
} elseif (empty($opts['mail_dst']) && ($action == 'create_mail_alias')) {
        echo "The destination mail is required to create the alias " .
				"(--mail_dst user@example.com)\n";
        exit (0);
} elseif (empty($opts['dom_dst']) && ($action == 'create_dom_alias')) {
        echo "The destination domain is required to create the alias " .
				"and already has to be created " .
				"(--dom_dst example.com)\n";
        exit (0);
} elseif (empty($opts['db_name']) && (($action == 'create_db') || ($action == 'create_db_user'))) {
        echo "The database name is required " .
				"(--db_name example)\n";
        exit (0);
} elseif (empty($opts['subdomain']) && ($action == 'create_subdomain')) {
        echo "The subdomain name is required " .
				"(--subdomain example)\n";
        exit (0);
} elseif (empty($opts['limit']) && ($action == 'set_mail_limit')) {
        echo "The limit is required " .
				"(--limit 10 or if you need to disable a limit, it has to be --1 according POSIX)\n";
        exit (0);
}


//Required fields depends on actions, one of
//	domain, password, admin_type, created_by, email, user...
$domain = $opts['domain'];
$password = $opts['password'];
$password_hash = md5($opts['password']);
$created_by = $opts['created_by'];
$user = $opts['user'];
$mail_dst = $opts['mail_dst'];
$dom_dst = $opts['dom_dst'];
$db_name = $opts['db_name'];
$subdomain = $opts['subdomain'];
$user_lang = $opts['user_lang'];
$plan_owner = $opts['plan_owner'];
$limit = $opts['limit'];
$fname="";
$lname="";
$firm="";
$zip="";
$city="";
$state="";
$country="";
$mail=$opts['mail'];
$phone="";
$fax="";
$street1="";
$street2="";
$customer_id="";
$gender="";

//Adding domain
$domain_admin_id="0";
$domain_mailacc_limit="0";
$domain_ftpacc_limit="0";
$domain_traffic_limit="5120";
$domain_sqld_limit="0";
$domain_sqlu_limit="0";
$domain_status="toadd";
$domain_subd_limit="0";
$domain_alias_limit="0";
$domain_ip_id=$opts['domain_ip_id'];
$domain_disk_limit="500";
$domain_php="yes";
$domain_cgi="no";

//Apply hosting plan defaults
$hosting_plan=$opts['hosting_plan'];

/*TODO After 1.0.2 lock is done with FLOCK
if (check_for_lock_file()) {
	echo "Error, system is locked";
	//TODO remove this send_request if it's locked it should be autoremoved
	exit(0);
}
*/

//DB Connection
$mysqli = new mysqli($imscpdb_host, $imscpdb_user, $imscpdb_password, $imscpdb_name);
if (mysqli_connect_errno()) {
	printf("Connect failed: %s\n", mysqli_connect_error());
	exit(1);
}

//Ispcp config
$cfg = ispCP_Registry::get('Config');

switch ($action) {
		case "create_domain":
		
		//Requires $domain,$password
		echo "Creating the default user for $domain \n";
		
		//Domain verification
		$query="SELECT `domain_name` FROM `domain` WHERE `domain_name`='$domain'";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
			echo "ERROR: You are trying to create an already existant domain\n";
			$result->close();
			exit(0);
		}

		//Reseller verification
		$query="SELECT `admin_id` FROM `admin` WHERE `admin_name`='$created_by'";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
		$row=$result->fetch_row();
		$created_by=$row[0];
		$result->close();
		} else {
			echo "ERROR: You are trying to use a non-existant admin\n";
			exit(0);
		}
		
		//Hosting plan verification
		if ($plan_owner == 'admin') {
			//Admin will be the resseller_id number 1
			$result = $mysqli->query("SELECT `props` FROM `hosting_plans` WHERE `name`='$hosting_plan' AND `reseller_id`='1'");
		} elseif ($plan_owner == 'reseller') {
			$result = $mysqli->query("SELECT `props` FROM `hosting_plans` WHERE `name`='$hosting_plan' AND `reseller_id`='$created_by'");
		} else {
			echo "ERROR: The plan you are trying to apply doesn't exist";
		}
		if ($result->num_rows>0) {
			$row=$result->fetch_row();
			$hosting_plan=$row[0];
			$result->close();
		} else {
			echo "ERROR: You are trying to use a non-existant hosting plan\n";
			exit(0);
		}

		//TODO hosting ip verification

		//The hosting plan properties are stored sepparated by commas
		$props = preg_split('/;/', $hosting_plan, -1, PREG_SPLIT_NO_EMPTY);
		
		$domain_php=trim($props[0], "_");
		$domain_cgi=trim($props[1], "_");
		$domain_subd_limit=$props[2];
		$domain_alias_limit=$props[3];
		$domain_mailacc_limit=$props[4];
		$domain_ftpacc_limit=$props[5];
		$domain_sqld_limit=$props[6];
		$domain_sqlu_limit=$props[7];
		$domain_traffic_limit=$props[8];
		$domain_disk_limit=$props[9];
		$domain_backups=trim($props[10], "_");
		$domain_manual_dns=trim($props[11], "_");
		
		//Inserting the admin data
		//the field `state` is only after 1.0.0
		$query = "
				INSERT INTO `admin` (
					`admin_name`, `admin_pass`, `admin_type`, `domain_created`,
					`created_by`, `fname`, `lname`,
					`firm`, `zip`, `city`,
					`country`, `email`, `phone`,
					`fax`, `street1`, `street2`,
					`customer_id`, `gender`
				)
				VALUES (
					?, ?, 'user', unix_timestamp(),
					?, ?, ?,
					?, ?, ?,
					?, ?, ?,
					?, ?, ?,
					?, ?
				)
			"; 
	
		if ($stmt = $mysqli->prepare($query)) {
		
			$stmt->bind_param('ssssssssssssssss',
				$domain,
				$password_hash,
				$created_by,
				$fname,
				$lname,
				$firm,
				$zip,
				$city,
				$country,
				$mail,
				$phone,
				$fax,
				$street1,
				$street2,
				$customer_id,
				$gender);
			if (!$stmt->execute()) {
				printf("ERROR: %s\n", $mysqli->error);
				exit(0);
			}
			$domain_admin_id = $mysqli->insert_id;
			$stmt->close();
		} else {
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
		
		echo "User created\n";
		//Inserting the domain data
		//domain_admin_id must be taken from previous query field admin_id.
		if (!empty($domain_admin_id)) {
			$query = "
				INSERT INTO `domain` (
					`domain_name`, `domain_admin_id`,
					`domain_created_id`, `domain_created`,
					`domain_mailacc_limit`, `domain_ftpacc_limit`,
					`domain_traffic_limit`, `domain_sqld_limit`,
					`domain_sqlu_limit`, `domain_status`,
					`domain_subd_limit`, `domain_alias_limit`,
					`domain_ip_id`, `domain_disk_limit`,
					`domain_disk_usage`, `domain_php`, `domain_cgi`,
					`allowbackup`, `domain_dns`
				)
				VALUES (
					?, ?,
					?, unix_timestamp(),
					?, ?,
					?, ?,
					?, ?,
					?, ?,
					?, ?, 
					'0', ?, ?,
					?, ?
				)
				"; 
			
			if ($stmt = $mysqli->prepare($query)) {
			
				$stmt->bind_param('sssssssssssssssss',
					$domain,
					$domain_admin_id,
					$created_by,
					$domain_mailacc_limit, 
					$domain_ftpacc_limit,
					$domain_traffic_limit, 
					$domain_sqld_limit,
					$domain_sqlu_limit, 
					$domain_status,
					$domain_subd_limit, 
					$domain_alias_limit,
					$domain_ip_id,
					$domain_disk_limit,
					$domain_php, 
					$domain_cgi,
					$domain_backups,
					$domain_manual_dns
					);
		
			if (!$stmt->execute()) {
				printf("ERROR: %s\n", $mysqli->error);
				exit(0);
			}
			$stmt->close();
			
			//Assign to the user a default language
			$query = "INSERT INTO user_gui_props (user_id,lang,layout,logo) VALUES " .
 				"('".$domain_admin_id."','".$user_lang."','omega_original','".$created_by.".png')";
			if (!($mysqli->query($query))) {
					echo "ERROR: The language for user ".$domain." could not be set\n";
					$result->close();
					exit(0);
			}			
							
			//To create the default addresses, there is the function create_default_mail
			
			} else {
				printf("ERROR: %s\n", $mysqli->error);
				exit(0);
			}
		} else {
			//TODO, tell the reason it could not be created (domain already exists ..)
			echo "ERROR: There has been a problem with the creation\n";
		}

	break;
	case "create_default_mail":
		//Requires $domain, OPTIONAL $dom_dst will be set to the parent domain
		if (empty($dom_dst)) {
			echo "Creating the default mailboxes webmaster, postmaster and abuse for $domain\n";
			$alias='';
			$dom_parent = $domain;
		} else {
			echo "Creating the default mailboxes webmaster, postmaster and abuse for $domain ".
				"that is an alias for $dom_dst\n";
			$alias='alias';
			$dom_parent = $dom_dst;
			//TODO TOFIX, still does not work wit alias, altough with some changes to dovecot
			//		and postfix it is possible
			echo "ERROR: Sorry, still doesn't work with domain aliases\n";
			echo "If you patched the dovecot/postfix config to accept mail for the " .
				"domain aliases it should work anyway";
			exit(0);
		}

		//Lookup for the admin name
		$query = "SELECT `admin_id` FROM `admin` WHERE `admin_name`='$dom_parent'";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$user_id=$row['admin_id'];
			$result->close();
		} else {
			echo "ERROR: You are trying to use a non existant domain\n";
			exit(0);
		}
		
		//Check if the adresses already exist
		$query = "SELECT mail_acc FROM mail_users " .
			"INNER JOIN domain ON domain.domain_id = mail_users.domain_id ".
			"WHERE ((mail_acc = 'webmaster') OR (mail_acc = 'abuse') OR (mail_acc = 'postmaster')) ".
			"AND domain.domain_name = '".$domain."'";
		$result = $mysqli->query($query);
		if ($result->num_rows != 0) {
			echo "ERROR: The default addresses are already created\n";
			exit(0);
		}
		
		//Lookup for the admin address
		$query = "SELECT email,domain_id FROM admin ". 
			"INNER JOIN domain ON admin.admin_id = domain.domain_admin_id " .
			"WHERE domain.domain_name = '" . $dom_parent . "' ";
 		$result = $mysqli->query($query);
		if ($result->num_rows == 0) {
			echo "ERROR: There was a problem, the domain should have admin email defined\n";
			exit(0);
		}
		$act_id = $mysqli->insert_id;
		$row = $result->fetch_array();
		$user_email = $row['email'];
		$domain_id=$row['domain_id'];
		//Some variables need to be stored on the session so we can use ispcp functions
		$_SESSION['user_id'] = $user_id;
		$_SESSION['user_email'] = $user_email;
		
		client_mail_add_default_accounts($domain_id, $user_email, $domain, $alias, $act_id);
		
	break;
	case "create_dom_alias":
		//Requires $domain, $dom_dst
		echo "Creating an alias of $dom_dst named $domain\n";
		
		//Some code taken from gui/reseller/alias_order.php
		
		//Destination domain verification (it MUST exist)
		$query = "SELECT domain_id FROM domain WHERE domain_name='" . $dom_dst . "'" ;
		if ($result = $mysqli->query($query)) {
			if ($result->num_rows == 0) {
				echo "ERROR: You MUST have the destination domain ". $dom_dst ." before doing an alias\n";
				$result->close();
				exit(0);
			}
		} else {
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
		
		$row=$result->fetch_array();
		$domain_id=$row['domain_id'];
		
		//Domain alias verification
		$result = $mysqli->query("SELECT `alias_name` FROM `domain_aliasses` WHERE `alias_name`='$domain'");
		if ($result->num_rows>0) {
			echo "ERROR: You are trying to create an already existant domain alias\n";
			$result->close();
			exit(0);
		}
		
		//To create webmaster, postmaster, abuse use client_mail_add_default_accounts
		//TODO, check for the case where the alias is only pending activation.
		//Insert the alias of the domain.
		//	$result = $mysqli->query("SELECT `alias_name` FROM `domain_aliasses` WHERE `alias_name`='$domain'");
		$query = "INSERT INTO `domain_aliasses` (`domain_id`, `alias_name`, `alias_mount`, `alias_status`, `alias_ip_id`, `url_forward`) ".
 			"VALUES ('".$domain_id."','".$domain."','/','toadd','1','http://www.".$dom_dst."/')";
		if ($mysqli->query($query)) {
			echo "Insert of alias successful \n";
		} else {
			echo "There was an error while creating the domain alias\n";
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
                $cmd = "/var/www/ispcp/engine/ispcp-rqst-mngr";
                exec ($cmd,$output);
		//The default mailboxes are created with the create_default_mail

	break;
	case "create_subdomain":
		//Requires $domain, $subdomain
		echo "Creating the subdomain $subdomain for $domain\n";
		
		//Some code taken from gui/client/subdomain_add.php
		
		//Destination domain verification (it MUST exist)
		$query = "SELECT domain_id FROM domain WHERE domain_name='" . $domain . "'" ;
		if ($result = $mysqli->query($query)) {
			if ($result->num_rows == 0) {
				echo "You MUST have de destination domain ". $domain ." before creating a subdomain\n";
				$result->close();
				exit(0);
			}
		} else {
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
		$row=$result->fetch_array();
		$domain_id=$row['domain_id'];
		
		//TODO, check if the subdomain exists
		$query = "SELECT subdomain_name FROM subdomain WHERE subdomain_name = '" . $subdomain . "'" ;
		if ($result = $mysqli->query($query)) {
			if ($result->num_rows>0) {
				echo "ERROR: You are trying to create an already existant subdomain\n";
				$result->close();
				exit(0);
			}
		} else {
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
		
		//Inserting the subdomain into the database
		$query = "INSERT INTO `subdomain`(`domain_id`, `subdomain_name`, " .
			"`subdomain_mount`, `subdomain_status`) " .
			"VALUES ('" . $domain_id. "', '" . $subdomain . "','/" . $subdomain ."', 'toadd')";
		if ($mysqli->query($query)) {
			echo "Insert of subdomain successful \n";
		} else {
			echo "There was an error while creating the subdomain $subdomain\n";
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
		
		//TODO, in case it's needed, create the mountpoint
		
	break;
	case "create_ftp":
		//Requires $domain, $password
		//Optional $user
		//TODO At the moment it only creates the main FTP user (only for tha main dmn)
		//	it could be improved to work also with 'als' and 'sub'
		//TODO, it doesn't do custom mountpoints, this could be parametrized
		//Domain verification
		$query = "SELECT `domain_admin_id` FROM domain WHERE domain_name = '".$domain."'";
		$result = $mysqli->query($query);
		if ($result->num_rows==0) {
			echo "ERROR: You are trying to use a non existant domain\n";
			$result->close();
			exit(0);
		} else {
			$row=$result->fetch_array();
			$admin_id = $row['domain_admin_id'];
		}

		if (empty($user)) {
		  $_POST['username'] = str_replace(".", "", $domain);
		} else {
		  $_POST['username'] = str_replace(".", "", $user);
		}

		//Ftp user verification
		$query = "SELECT userid FROM ftp_users WHERE userid = '" . 
			$user . "@" . $domain . "'";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
			echo "ERROR: You are trying to create an already existant FTP user\n";
			$result->close();
			exit(0);
		}		

		$ftp_user = $user . $cfg->FTP_USERNAME_SEPARATOR . $domain;
		$ftp_home = $cfg->FTP_HOMEDIR . "/$domain";

		$ftp_gid = get_ftp_user_gid($sql, $domain, $admin_id, $ftp_user);
		$ftp_uid = get_ftp_user_uid($sql, $domain, $admin_id, $ftp_user, $ftp_gid);
		
		$ftp_shell = $cfg->CMD_SHELL;
		$ftp_passwd = crypt_user_pass_with_salt($password);

		$query = "
			INSERT INTO ftp_users
				(`userid`, `passwd`, `uid`, `gid`, `shell`, `homedir`)
			VALUES
				(?, ?, ?, ?, ?, ?)
		";

		$rs = exec_query($sql, $query, array($ftp_user, $ftp_passwd, $ftp_uid, $ftp_gid, $ftp_shell, $ftp_home));

		write_log($domain . ": add new FTP account: $ftp_user");

	break;
	case "create_mail":
		//Requires $domain, $user, $password
		//Optional $mail_dst if it has also a redirection
		echo "Creating the mailbox $user@$domain\n";
		
		//Domain verification
		$result = $mysqli->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
		if ($result->num_rows>0) {
		  $row=$result->fetch_row();
		  $domain_id=$row[0];
		  $result->close();
		  } else {
			  echo "ERROR: You are trying to use a non-existant domain\n";
			  exit(0);
		}

      //Email verification
      $result = $mysqli->query("SELECT `mail_addr` FROM `mail_users` WHERE `mail_addr`='$user@$domain'");
		if ($result->num_rows !=0) {
			echo "ERROR: You are trying to create an existant mail address\n";
			exit(0);
		}
		
		if ($mail_dst != '') {
			$mail_type = "normal_mail,normal_forward";
			$mail_forw = $mail_dst;
		} else {
			$mail_type = "normal_mail";
			$mail_forw = "_no_";
		}
		
		//TODO, allow the quota to be set with a parameter
		$enc_pass =  encrypt_db_password($password);
		$sql = "INSERT INTO `mail_users` VALUES ('','$user','$enc_pass','" . $mail_forw . 
			"','$domain_id','" . $mail_type ."',0,'toadd',0,'',0,'$user@$domain')";
		//echo $sql . "\n";

		//Adding the user
		if (!$mysqli->query($sql)) {
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}

	break;
	case "create_mail_alias":
		//Requires $domain, $user, $mail_dst, 
		echo "Creating the mailbox alias $user@$domain pointing to $mail_dst\n";
				
		//Domain verification
		$result = $mysqli->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
      if ($result->num_rows>0) {
			$row=$result->fetch_row();
			$domain_id=$row[0];
			$result->close();
         } else {
         	echo "ERROR: You are trying to use a non-existant domain\n";
            	exit(0);
      }
		
		//Email verification
      $result = $mysqli->query("SELECT `mail_addr` FROM `mail_users` WHERE `mail_addr`='$user@$domain'");
		if ($result->num_rows !=0) {
			echo "ERROR: You are trying to create an existant mail address\n";
			exit(0);
		}
		
		//TODO, allow the quota to be set with a parameter
		//Adding the alias		
		$sql = "INSERT INTO `mail_users` VALUES  ('','" . $user . "','_no_" . 
			"','" . $mail_dst . "','" . $domain_id . "','normal_forward',0,'toadd',0,'',0,' " .
 			$user . "@" . $domain . "')";
		//echo $sql . "\n";

		//Adding the user
		if (!$mysqli->query($sql)) {
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}

	break;
	case "fix_htdocs_owner":
		//Requires $domain
		echo "Fixing the permissions of the web directory for $domain \n";
		
		//Fix the htdocs owner so it's the same as in this computer
		//	useful when migrating from another machine	

		//Domain verification
		$result = $mysqli->query("SELECT `domain_name` FROM `domain` WHERE `domain_name`='$domain'");
		if ($result->num_rows == 0) {
			echo "ERROR: You cannot change permisions on a non-existant domain";
			$result->close();
			exit(0);
		}		

		//Obtain uid and gid
		$query="SELECT `domain_uid`,`domain_gid` FROM `domain` WHERE `domain_name`='$domain'";
		if ($result = $mysqli->query($query)) {;
			if ($result->num_rows > 0) {
				$row=$result->fetch_array();
				$uid=$row['domain_uid'];
				$gid=$row['domain_gid'];
				//TODO, do the same for htsdocs in case it's used
				$cmd =  "chown -R vu" . $uid . ":vu" . $gid . " /var/www/virtual/" . $domain . "/htdocs/"; 
				exec ($cmd,$output);
				$cmd =  "chmod 775 /var/www/virtual/" . $domain . "/htdocs";
				//It might be necessary to change the permissions in /statistics too
				exec ($cmd,$output);
				$result->close();
			} else {
				echo "ERROR: There was an error obtaining the user uid and gid\n";
				exit(0);
			}
		} else {
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
		
	break;
	case "create_db":
		//Requires $domain, $db_name
		echo "Creating the database $db_name in the domain $domain \n";
		//Domain verification
		$result = $mysqli->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
      if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$domain_id=$row['domain_id'];
			$result->close();
         } else {
				echo "ERROR: You are trying to use a non-existant domain\n";
				exit(0);
		}

		//Database verification
		$query = "SELECT `sqld_name` FROM `sql_database` WHERE `sqld_name`='$db_name'";
		$result = $mysqli->query($query);
      if ($result->num_rows>0) {
			echo "ERROR: You are trying create an already existant database\n";
			echo "Reloading mysql anyway (in case you moved via rsync the data)\n";
			$cmd = "/etc/init.d/mysql reload\n"; 
			exec ($cmd,$output);
			exit(0);
		}

		//Query to insert the database to the control panel
		$query = "INSERT INTO sql_database (domain_id, sqld_name) " .
			"VALUES ('" . $domain_id . "', '" . $db_name . "')";		
		if ($mysqli->query($query)) {
			echo "Creation of the database in ispcp successful \n";
			echo "Reloading the database server (in case you moved via rsync the data)\n";
			$cmd = "/etc/init.d/mysql reload"; 
			exec ($cmd,$output);
		} else {
			echo "There was an error while creating the database in ispcp\n";
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
			
	break;
	case "create_db_user":
		//Requires $domain, $db_name, $user, $password
		echo "Creating the database user $user for the database $db_name in the domain $domain\n";
		//Domain verification
		$query = "SELECT sqld_id FROM sql_database " .
 			"INNER JOIN domain ON domain.domain_id = sql_database.domain_id " . 
			"WHERE `domain_name`='" . $domain . "' AND sqld_name = '" . $db_name ."'";
		if ($result = $mysqli->query($query)) {
			if ($result->num_rows>0) {
				$row=$result->fetch_array();
				$sqld_id=$row['sqld_id'];
				$result->close();
			} else {
					echo "ERROR: You are trying to use a non-existant domain or the database $db_name ".
						"has not been created\n";
					exit(0);
			}
		} else {
			echo "There was an error while creating the database in ispcp\n";
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
		
		//Database verification
		$query = "SELECT * FROM sql_user ".
			"INNER JOIN sql_database ON sql_database.sqld_id = sql_user.sqld_id ".
			"WHERE sqlu_name = '".$user."' ".
			"AND sqld_name = '".$db_name."';";
		$result = $mysqli->query($query);
      if ($result->num_rows>0) {
				echo "ERROR: You are trying create an already existant database user\n";
				exit(0);
		}
		
		//Query to insert the db_user to the control panel
		$enc_pass =  encrypt_db_password($password);
		$query = "INSERT INTO sql_user (sqld_id, sqlu_name, sqlu_pass) " .
			"VALUES ('" . $sqld_id . "', '" . $user . "', '" . $enc_pass . "')";
		if ($mysqli->query($query)) {
			echo "Creation of the database user in ispcp successful \n";
		} else {
			echo "There was an error while creating the database user in ispcp\n";
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
		
		//Grant the permissions of the database to the user
		$query = "USE mysql; " .
			"GRANT ALL PRIVILEGES ON ". $db_name .".* TO '". $user ."'@'%' " .
 			"IDENTIFIED BY '" . $password . "'; " .
			"GRANT ALL PRIVILEGES ON ". $db_name .".* TO '". $user ."'@localhost " .
 			"IDENTIFIED BY '" . $password . "'; " .
			"FLUSH PRIVILEGES;";
		echo $query . "\n";
		//TODO use mysqli_more_results to check for errors
		if ($mysqli->multi_query($query)) {
			echo "The user rights where succesfully granted \n";
		} else {
			echo "There was an error while granting the user righs for the database\n";
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
	break;
	case "create_htaccess_user":
		//Requires $domain $user $password
		//The recomendation would be to the same value of $domain as $user
		
		//Domain verification
		$result = $mysqli->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$domain_id=$row['domain_id'];
			$result->close();
         } else {
				echo "ERROR: The domain doesn't exist\n";
				exit(0);
		}

		$password_hash=crypt_user_pass_with_salt($password);
		$query = "INSERT INTO htaccess_users (`dmn_id`, `uname`, `upass`, `status`) ".
		  "VALUES ('".$domain_id."', '".$user."', '".$password_hash."', 'toadd');";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };

	break;
	case "join_htaccess_group":
		//Requires $domain, $user
		//Optional $group (default is statistics)
		//Domain verification
		$result = $mysqli->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$domain_id=$row['domain_id'];
			$result->close();
         } else {
				echo "ERROR: The domain doesn't exist\n";
				exit(0);
		}
		
		$query = "SELECT `id` FROM `htaccess_users` WHERE dmn_id='".$domain_id."' ".
		  "AND uname='".$user."'";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$user_id=$row['id'];
			$result->close();
         } else {
				echo "ERROR: You cannot regenerate a non-existant domain\n";
				exit(0);
		}
		
		$query = "SELECT id,members FROM htaccess_groups WHERE dmn_id='".$domain_id."' ".
			"AND ugroup='statistics'";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$group_id=$row['id'];
			//TODO Whe should check if $user_id is already part of the group (using explode)
			if ($row['members'] == '') {
			  $members = $user_id;
			} else {
			  $members=$row['members'].",".$user_id;
			}
			$query = "UPDATE htaccess_groups SET members='".$members."' WHERE id='".$group_id."';";
		} else {
			$members=$user_id;
         	$query = "INSERT INTO `htaccess_groups` (`dmn_id`, `ugroup`, `members`, `status`) ".
			  "VALUES ('".$domain_id."', 'statistics', '".$members."', 'toadd');";
		}
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };

	break;
	case "change_domain_pass":
		//Requires $domain, $password
		//Domain verification
		$query="SELECT `domain_admin_id` FROM `domain` WHERE `domain_name`='".$domain."';";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$admin_id=$row['domain_admin_id'];
			$result->close();
         } else {
				echo "ERROR: You cannot change the password for a non-existant domain\n";
				exit(0);
		}
		$password_hash=crypt_user_pass_with_salt($password);
		$query = "UPDATE `admin` SET `admin_pass` = '".$password_hash."' ".
			"WHERE `admin_id` = '".$admin_id."';";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };
		
	break;
        case "change_mailuser_pass":
                //Requires $mail, $password
                echo "Updating password of mailbox $mail\n";
                       
                //Email verification
                $result = $mysqli->query("SELECT `mail_addr` FROM `mail_users` WHERE `mail_addr`='$mail'");
                        if ($result->num_rows !=1) {
                                echo "ERROR: You are trying to update a non existant mail address\n";
                                exit(0);
                        }
                       
                        $enc_pass =  encrypt_db_password($password);
                        $sql = "UPDATE `mail_users` SET `mail_pass`= '".$enc_pass."' WHERE `mail_addr`='".$mail."';";
                        //echo $sql . "\n";
        
                        //Updating the user
                        if (!$mysqli->query($sql)) {
                                printf("ERROR: %s\n", $mysqli->error);
                                exit(0);
                        }

        break;
	case "set_mail_limit":
		//Requires $domain, $limit
		$query="SELECT `domain_id` FROM `domain` WHERE `domain_name`='".$domain."';";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$domain_id=$row['domain_id'];
			$result->close();
         } else {
				echo "ERROR: You cannot disable for a non-existant domain\n";
				exit(0);
		}
		
		$query="UPDATE `domain` SET domain_mailacc_limit='".$limit."' WHERE domain_id='".
			$domain_id."';";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };
		
	break;
	case "regenerate_domain":
		//Requires $domain
		//Domain verification
		$result = $mysqli->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$domain_id=$row['domain_id'];
			$result->close();
         } else {
				echo "ERROR: You cannot regenerate a non-existant domain\n";
				exit(0);
		}
		
		//Change to "change" the status on the database
		$query = "USE ispcp; " .
			"UPDATE `domain` SET `domain_status` = 'change' " .
			"WHERE `domain_status` = 'ok' AND domain_id = ".$domain_id."; " .
			"UPDATE `subdomain` SET `subdomain_status` = 'change' " .
			"WHERE `subdomain_status` = 'ok' AND domain_id = ".$domain_id."; " .
			"UPDATE `domain_aliasses` SET `alias_status` = 'change' ".
			"WHERE `alias_status` = 'ok' AND domain_id = ".$domain_id."; " .
			"UPDATE `mail_users` SET `status` = 'change' ".
			"WHERE `status` = 'ok' AND domain_id = ".$domain_id.";";

		//TODO use mysqli_more_results to check for errors
		if ($mysqli->multi_query($query)) {
			echo "The domain ".$domain." is scheduled for regeneration\n";
		} else {
			echo "There was an error while scheduling the regeneration of ".$domain."\n";
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}
		echo "#To launch the regeneration of $domain from console, you should do:\n";
		echo "/var/www/ispcp/engine/ispcp-rqst-mngr\n";
	break;
	case "delete_alias":
		//With this action, we will delete an alias from a domain_admin_id
		//Requires $domain, $dom_dst
		//Domain verification
		$query = "SELECT domain_id FROM `domain` ".
		  "WHERE `domain_name`='".$dom_dst."';";
		echo $query."\n";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$domain_id=$row['domain_id'];
			$result->close();
		} else {
				echo "ERROR: You cannot delete an alias from a non-existant domain\n";
				exit(0);
		}
		
		//Alias verification
		$query = "SELECT * FROM domain_aliasses ".
		  "WHERE alias_name='".$domain."' AND domain_id = '".$domain_id."'";
		$result = $mysqli->query($query);
		if ($result->num_rows != 1) {
				echo "ERROR: You cannot delete a non-existant alias\n";
				exit(0);
		} else {
		  $result->close();
		}
		
		$query = "UPDATE `domain_aliasses` SET `alias_status` = 'delete' ".
		  "WHERE alias_name = '".$domain."' AND domain_id = '".$domain_id."';";
		echo $query."\n";
 		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };
	break;
	case "delete_domain":
		//With this action, we will delete all the data from the user
		//	mailboxed, web, etc ...
		//Requires $domain
		//Domain verification
		$query = "SELECT domain_id,domain_uid,domain_gid,domain_admin_id FROM `domain` ".
		  "WHERE `domain_name`='".$domain."'";
		$result = $mysqli->query($query);
		if ($result->num_rows>0) {
			$row=$result->fetch_array();
			$domain_id=$row['domain_id'];
			$domain_uid=$row['domain_uid'];
			$domain_gid=$row['domain_gid'];
			$domain_admin_id=$row['domain_admin_id'];
			$result->close();
		} else {
				echo "ERROR: You cannot delete a non-existant domain\n";
				exit(0);
		}

		//Query to schdedule for deletion the domain
		$query = "UPDATE `mail_users` SET `status` = 'delete' ".
		  "WHERE `domain_id` = '".$domain_id."';";
		echo $query."\n";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };
		$query = "UPDATE `htaccess` SET `status` = 'delete' ".
		  "WHERE `dmn_id` = '".$domain_id."';";
		echo $query."\n";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };
		$query = "UPDATE `htaccess_groups` SET `status` = 'delete' ".
		  "WHERE `dmn_id` = '".$domain_id."';";
		echo $query."\n";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };
		$query = "UPDATE `htaccess_users` SET `status` = 'delete' ".
		  "WHERE `dmn_id` = '".$domain_id."';";
		echo $query."\n";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };
		$query = "UPDATE `domain_aliasses` SET `alias_status` = 'delete' ".
		  "WHERE `domain_id` = '".$domain_id."';";
		echo $query."\n";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };
		$query = "UPDATE `subdomain` SET `subdomain_status` = 'delete' ".
		  "WHERE `domain_id` = '".$domain_id."';";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };
		echo $query."\n";
		
		//Finally we mark the domain for removal
		$query = "UPDATE `domain` SET `domain_status` = 'delete' ".
		  "WHERE `domain_id` = '".$domain_id."';";
		echo $query . "\n";
		if (!($mysqli->query($query))) { echo "sql_error"; exit(0); };

		
		//Some stuff has to be removed afterwards
		// Delete FTP users
		// Delete ispcp login:
		// Delete the quota section:
		// Remove support tickets:
		$query = "USE ispcp; " .
		  "DELETE FROM `ftp_users` WHERE `uid` = '".$domain_uid."'; ".
		  "DELETE FROM `ftp_group` WHERE `gid` = '".$domain_gid."'; ".
		  "DELETE FROM `admin` WHERE `admin_id` = '".$domain_admin_id."'; ".
		  "DELETE FROM `quotalimits` WHERE `name` = '".$domain."'; ".
		  "DELETE FROM `tickets` WHERE ticket_from = '".$domain_admin_id."' OR ticket_to = '".
			$domain_admin_id."';";

		//TODO use mysqli_more_results to check for errors
		if ($mysqli->multi_query($query)) {
			echo "The domain ".$domain." is scheduled for deletion\n";
		} else {
			echo "There was an error while deleting ".$domain."\n";
			printf("ERROR: %s\n", $mysqli->error);
			exit(0);
		}

		// Delete the databases
		$query = "SELECT `sqld_id` FROM `sql_database` WHERE `domain_id` = '".$domain_id."';";
		echo $query."\n";
		if ($result = $mysqli->query($query)) {
			while ($row = $result->fetch_array()) {
			  $_SESSION['user_logged'] = $domain;
			  echo 'delete_sql_database(&$sql, '.$domain_id.", ".$row['sqld_id'].".)";
			  delete_sql_database(&$sql, $domain_id, $row['sqld_id']);
			}
		}
	break;
	default:
		echo "ERROR: You have selected a non existant action\n";
		exit(0);
	break;
}

//If we could reach this point, then we tell the engine to complete 
//	the process
send_request();
	
?>
