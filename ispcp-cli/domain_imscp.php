#!/usr/bin/php -q
<?php
/**
 *
 * @copyright 	2013 by jjuvan@grn.cat
 * @author 		jjuvan@grn.cat
 * @license
 *   This program is licensed under GPL. See COPYING for details
 */

ini_set('display_errors', E_ALL);
ini_set('display_errors', 1);


require_once 'bootstrap.php';
require_once 'calc-functions-custom.php';
require_once 'config.php';
require_once 'getopts.php';
require_once 'common_imscp.php';


function help() {
    echo "HELP!!";
    exit(0);
}

$opts = getopts(array(
    'action' => array('switch' => 'action', 'type' => GETOPT_VAL),
    'domain' => array('switch' => 'domain', 'type' => GETOPT_VAL),
    'password' => array('switch' => 'password', 'type' => GETOPT_VAL),
    'user_lang' => array('switch' => 'user_lang', 'type' => GETOPT_VAL, 'default' => $user_lang),
    'created_by' => array('switch' => 'created_by', 'type' => GETOPT_VAL, 'default' => $created_by),
    'mail' => array('switch' => 'mail', 'type' => GETOPT_VAL, 'default' => $default_mail),
    'domain_ip_id' => array('switch' => 'domain_ip_id', 'type' => GETOPT_VAL, 'default' => $default_ip_id),
    'hosting_plan' => array('switch' => 'hosting_plan', 'type' => GETOPT_VAL, 'default' => $default_hosting_plan),
    'plan_owner' => array('switch' => 'plan_owner', 'type' => GETOPT_VAL, 'default' => $plan_owner),
    'user' => array('switch' => 'user', 'type' => GETOPT_VAL),
    'dom_dst' => array('switch' => 'dom_dst', 'type' => GETOPT_VAL),
    'db_name' => array('switch' => 'db_name', 'type' => GETOPT_VAL),
    'subdomain' => array('switch' => 'subdomain', 'type' => GETOPT_VAL),
    'mail_dst' => array('switch' => 'mail_dst', 'type' => GETOPT_VAL),
    'limit' => array('switch' => 'limit', 'type' => GETOPT_VAL),
    'help' => array('switch' => 'help','type' => GETOPT_SWITCH)
),$_SERVER['argv']);

//Check for values
if (isset($opts['help']) && !empty($opts['help'])) {
    help();
    exit(0);
}
//TODO, check for --help flag and explain better the create_ftp/create_domain
if (empty($opts['action'])) {
    echo "Action required (--action) and one of: \n\tcreate_domain\n\tcreate_ftp " .
        "\n\tcreate_default_mail \n\tcreate_mail \n\tfix_htdocs_owner \n\tcreate_mail_alias " .
        "\n\tcreate_dom_alias \n\tcreate_subdomain \n\tcreate_db \n\tcreate_db_user " .
        "\n\tregenerate_domain \n\tdelete_domain \n\tdelete_alias ".
        "\n\tcreate_htaccess_user \n\tjoin_htaccess_group\n\tchange_domain_pass\n\tset_mail_limit\n";
    exit (0);
} else {
    $action=$opts['action'];
}

if (empty($opts['domain'])) {
    echo "Domain name required, will be the same as de admin user" .
        " for that domain  (--domain example.com)\n";
    exit (0);
} elseif (empty($opts['password']) && ($action != 'fix_htdocs_owner') && ($action != 'create_dom_alias')
         && ($action != 'create_mail_alias') && ($action != 'create_db') && ($action !=  'create_subdomain') && ($action != 'regenerate_domain') && ($action != 'create_default_mail') && ($action != 'delete_domain') && ($action != 'delete_alias') && ($action != 'join_htaccess_group') && ($action != 'set_mail_limit')) {
    echo "Password required (--password)\n";
    exit (0);
} elseif ((empty($opts['created_by'])  && ($action == 'create_domain'))) {
    echo "Created_by is required (--created_by)\n";
    exit (0);
} elseif (empty($opts['mail'])) {
    echo "Email is required\n";
    exit (0);
} elseif (empty($opts['domain_ip_id'])) {
    echo "Domain ip id required\n";
    exit (0);
} elseif (empty($opts['hosting_plan'])) {
    echo "Hosting plan is required (--hosting_plan)\n";
    exit (0);
} elseif (empty($opts['plan_owner'])) {
    echo "Plan owner (admin or reseller) is required (--plan_owner)\n";
    exit (0);
} elseif (empty($opts['user']) && (($action == 'create_mail') || ($action == 'create_db_user') || ($action == 'join_htaccess_group') || ($action == 'create_htaccess_user') || ($action == 'create_mail_alias'))) {
    echo "The user is required (--user)\n";
    exit (0);
} elseif (empty($opts['mail_dst']) && ($action == 'create_mail_alias')) {
    echo "The destination mail is required to create the alias " .
                "(--mail_dst user@example.com)\n";
    exit (0);
} elseif (empty($opts['dom_dst']) && (($action == 'create_dom_alias') || ($action == 'delete_alias'))) {
    echo "The destination domain is required to create the alias " .
                "and already has to be created " .
                "(--dom_dst example.com)\n";
    exit (0);
} elseif (empty($opts['db_name']) && (($action == 'create_db') || ($action == 'create_db_user'))) {
    echo "The database name is required " .
                "(--db_name example)\n";
    exit (0);
} elseif (empty($opts['subdomain']) && ($action == 'create_subdomain')) {
    echo "The subdomain name is required " .
                "(--subdomain example)\n";
    exit (0);
} elseif (empty($opts['limit']) && ($action == 'set_mail_limit')) {
    echo "The limit is required " .
                "(--limit 10 or if you need to disable a limit, it has to be --1 according POSIX)\n";
    exit (0);
}


//Required fields depends on actions, one of
//	domain, password, admin_type, created_by, email, user...
$domain = $opts['domain'];
$password = $opts['password'];
$password_hash = md5($opts['password']);
$created_by = $opts['created_by'];
$user = $opts['user'];
$mail_dst = $opts['mail_dst'];
$dom_dst = $opts['dom_dst'];
$db_name = $opts['db_name'];
$subdomain = $opts['subdomain'];
if (validate_lang($opts['user_lang']))
	$user_lang = $opts['user_lang'];
else {
	debug("$user_lang is not a valid language defaulting to locale en_UK");
	$user_lang='en_UK';
}
$plan_owner = $opts['plan_owner'];
$limit = $opts['limit'];
$fname="";
$lname="";
$firm="";
$zip="";
$city="";
$state="";
$country="";
$mail=$opts['mail'];
$phone="";
$fax="";
$street1="";
$street2="";
$customer_id="";
$gender="";

//Adding domain
$domain_admin_id="0";
$domain_mailacc_limit="0";
$domain_ftpacc_limit="0";
$domain_traffic_limit="5120";
$domain_sqld_limit="0";
$domain_sqlu_limit="0";
$domain_status="toadd";
$domain_subd_limit="0";
$domain_alias_limit="0";
$domain_ip_id=$opts['domain_ip_id'];
$domain_disk_limit="500";
$domain_php="yes";
$domain_cgi="no";

//Apply hosting plan defaults
$hosting_plan=$opts['hosting_plan'];

//Initializing the system config
$cfg = iMSCP_Registry::get('config');

//We retrive the database connection from imscp
$pdo = iMSCP_Registry::get('db');

//We can't start while rqst-mngr is running
wait_rqst();

switch ($action) {
        case "create_domain":

        //Requires $domain,$password
        info("Creating the default user for $domain");

        //Domain verification
        $query="SELECT `domain_name` FROM `domain` WHERE `domain_name`='$domain'";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            error("The domain $domain already exists");
            exit(0);
        }

        //Domain admin verification
        $query="SELECT `admin_name` FROM `admin` WHERE `admin_name`='$domain'";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            echo "ERROR: The domain admin for $domain already exists\n";
            exit(0);
        }

        //Reseller verification
        $query="SELECT `admin_id` FROM `admin` WHERE `admin_name`='$created_by'";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            $row=$result->fetch();
            $created_by=$row[0];
        } else {
            echo "ERROR: You are trying to use a non-existant admin\n";
            exit(0);
        }

        //Hosting plan verification
        if ($plan_owner == 'admin') {
            //Admin will be the resseller_id number 1
            $result = $pdo->query("SELECT `props` FROM `hosting_plans` WHERE `name`='$hosting_plan' AND `reseller_id`='1'");
        } elseif ($plan_owner == 'reseller') {
            $result = $pdo->query("SELECT `props` FROM `hosting_plans` WHERE `name`='$hosting_plan' AND `reseller_id`='$created_by'");
        } else {
            echo "ERROR: The plan you are trying to apply doesn't exist";
        }
        if ($result->rowCount() > 0) {
            $row=$result->fetch();
            $hosting_plan=$row[0];
        } else {
            echo "ERROR: You are trying to use a non-existant hosting plan\n";
            exit(0);
        }

        //Hosting ip verification
        $result = $pdo->query("SELECT `ip_number` FROM `server_ips` WHERE `ip_id`='$default_ip_id'");
        if ($result->rowCount() == 0) {
            echo "ERROR: You are trying to use a non-existant ip address\n";
            exit(0);
        }



        //The hosting plan properties are stored sepparated by commas
        $props = preg_split('/;/', $hosting_plan, -1, PREG_SPLIT_NO_EMPTY);

        $domain_php=trim($props[0], "_");
        $domain_cgi=trim($props[1], "_");
        $domain_subd_limit=$props[2];
        $domain_alias_limit=$props[3];
        $domain_mailacc_limit=$props[4];
        $domain_ftpacc_limit=$props[5];
        $domain_sqld_limit=$props[6];
        $domain_sqlu_limit=$props[7];
        $domain_traffic_limit=$props[8];
        $domain_disk_limit=$props[9];
        $domain_backups=trim($props[10], "_");
        $domain_manual_dns=trim($props[11], "_");

        //Inserting the domain admin data
        $query = "
                INSERT INTO `admin` (
                    `admin_name`, `admin_pass`, `admin_type`, `domain_created`,
                    `created_by`, `fname`, `lname`,
                    `firm`, `zip`, `city`,`state`,
                    `country`, `email`, `phone`,
                    `fax`, `street1`, `street2`,
                    `customer_id`, `gender`,`admin_status`
                )
                VALUES (
                    ?, ?, 'user', unix_timestamp(),
                    ?, ?, ?,
                    ?, ?, ?,?,
                    ?, ?, ?,
                    ?, ?, ?,
                    ?, ?, 'toadd'
                )
            ";

        if ($stmt = $pdo->prepare($query)) {
            $stmt->bindValue(1,$domain,PDO::PARAM_STR);
            $stmt->bindValue(2,$password_hash,PDO::PARAM_STR);
            $stmt->bindValue(3,$created_by,PDO::PARAM_STR);
            $stmt->bindValue(4,$fname,PDO::PARAM_STR);
            $stmt->bindValue(5,$lname,PDO::PARAM_STR);
            $stmt->bindValue(6,$firm,PDO::PARAM_STR);
            $stmt->bindValue(7,$zip,PDO::PARAM_STR);
            $stmt->bindValue(8,$city,PDO::PARAM_STR);
            $stmt->bindValue(9,$city,PDO::PARAM_STR);
            $stmt->bindValue(10,$country,PDO::PARAM_STR);
            $stmt->bindValue(11,$mail,PDO::PARAM_STR);
            $stmt->bindValue(12,$phone,PDO::PARAM_STR);
            $stmt->bindValue(13,$fax,PDO::PARAM_STR);
            $stmt->bindValue(14,$street1,PDO::PARAM_STR);
            $stmt->bindValue(15,$street2,PDO::PARAM_STR);
            $stmt->bindValue(16,$customer_id,PDO::PARAM_STR);
            $stmt->bindValue(17,$gender,PDO::PARAM_STR);
            if (!$stmt->execute()) {
                printf("ERROR: %s\n", $pdo->error);
                exit(0);
            }
            $domain_admin_id = $pdo->lastInsertId();
        } else {
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

        info("User created");
        //Inserting the domain data
        //domain_admin_id must be taken from previous query field admin_id.
        if (!empty($domain_admin_id)) {
            $query = "
                INSERT INTO `domain` (
                    `domain_name`, `domain_admin_id`,
                    `domain_created_id`, `domain_created`,
                    `domain_mailacc_limit`, `domain_ftpacc_limit`,
                    `domain_traffic_limit`, `domain_sqld_limit`,
                    `domain_sqlu_limit`, `domain_status`,
                    `domain_subd_limit`, `domain_alias_limit`,
                    `domain_ip_id`, `domain_disk_limit`,
                    `domain_disk_usage`, `domain_php`, `domain_cgi`,
                    `allowbackup`, `domain_dns`
                )
                VALUES (
                    ?, ?,
                    ?, unix_timestamp(),
                    ?, ?,
                    ?, ?,
                    ?, ?,
                    ?, ?,
                    ?, ?,
                    '0', ?, ?,
                    ?, ?
                )

            ";

            if ($stmt = $pdo->prepare($query)) {
                $stmt->bindValue(1,$domain,PDO::PARAM_STR);
                $stmt->bindValue(2,$domain_admin_id,PDO::PARAM_STR);
                $stmt->bindValue(3,$created_by,PDO::PARAM_STR);
                $stmt->bindValue(4,$domain_mailacc_limit,PDO::PARAM_STR);
                $stmt->bindValue(5,$domain_ftpacc_limit,PDO::PARAM_STR);
                $stmt->bindValue(6,$domain_traffic_limit,PDO::PARAM_STR);
                $stmt->bindValue(7,$domain_sqld_limit,PDO::PARAM_STR);
                $stmt->bindValue(8,$domain_sqlu_limit,PDO::PARAM_STR);
                $stmt->bindValue(9,$domain_status,PDO::PARAM_STR);
                $stmt->bindValue(10,$domain_subd_limit,PDO::PARAM_STR);
                $stmt->bindValue(11,$domain_alias_limit,PDO::PARAM_STR);
                $stmt->bindValue(12,$domain_ip_id,PDO::PARAM_STR);
                $stmt->bindValue(13,$domain_disk_limit,PDO::PARAM_STR);
                $stmt->bindValue(14,$domain_php,PDO::PARAM_STR);
                $stmt->bindValue(15,$domain_cgi,PDO::PARAM_STR);
                $stmt->bindValue(16,$domain_backups,PDO::PARAM_STR);
                $stmt->bindValue(17,$domain_manual_dns,PDO::PARAM_STR);

            if (!$stmt->execute()) {
                printf("ERROR: %s\n", $pdo->error);
                exit(0);
            }
            $domain_id = $pdo->lastInsertId();

            //Assign the default php_ini properties to the domain
            $query = "INSERT INTO `php_ini` (
                                        `disable_functions`, `allow_url_fopen`, `display_errors`, `error_reporting`, `post_max_size`,
                                        `upload_max_filesize`, `max_execution_time`, `max_input_time`, `memory_limit`, `domain_id`
                                ) VALUES (
                                        'show_source,system,shell_exec,passthru,exec,phpinfo,shell,symlink', 'off', 'off', '30711',
                    '0', '0', '0', '0', '128', '".$domain_id."');";
            if (!($pdo->query($query))) {
                    echo "ERROR: The php_ini attribs for domain ".$domain." could not be set\n";
                    exit(0);
            }

            //Assign to the user a default language
            $query = "INSERT INTO user_gui_props (user_id,lang,layout,logo) VALUES " .
                 "('".$domain_admin_id."','".$user_lang."','omega_original','".$created_by.".png')";
            if (!($pdo->query($query))) {
                    echo "ERROR: The language for user ".$domain." could not be set\n";
                    exit(0);
            }

            //To create the default addresses, there is another function create_default_mail
            } else {
                printf("ERROR: %s\n", $pdo->error);
                exit(0);
            }
        } else {
            //TODO, tell the reason it could not be created (domain already exists ..)
            error("ERROR: There has been a problem with the creation");
        }

    break;
    case "create_default_mail":
        //Requires $domain, OPTIONAL $dom_dst will be set to the parent domain
        if (empty($dom_dst)) {
            info("Creating the default mailboxes webmaster, postmaster and abuse for $domain");
            $alias='';
            $dom_parent = $domain;
        } else {
            info("Creating the default mailboxes webmaster, postmaster and abuse for $domain ".
                "that is an alias for $dom_dst");
            $alias='alias';
            $dom_parent = $dom_dst;
            //TODO TOFIX, still does not work wit alias, altough with some changes to dovecot
            //		and postfix it is possible
            echo "ERROR: Sorry, still doesn't work with domain aliases\n";
            echo "If you patched the dovecot/postfix config to accept mail for the " .
                "domain aliases it should work anyway";
            exit(0);
        }

        //Lookup for the admin name
        $query = "SELECT `admin_id` FROM `admin` WHERE `admin_name`='$dom_parent'";
        $result = $pdo->query($query);
        if ($result->rowCount()>0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $user_id=$row['admin_id'];
        } else {
            echo "ERROR: You are trying to use a non existant domain\n";
            exit(0);
        }

        //Check if the adresses already exist
        $query = "SELECT mail_acc FROM mail_users " .
            "INNER JOIN domain ON domain.domain_id = mail_users.domain_id ".
            "WHERE ((mail_acc = 'webmaster') OR (mail_acc = 'abuse') OR (mail_acc = 'postmaster')) ".
            "AND domain.domain_name = '".$domain."'";
        $result = $pdo->query($query);
        if ($result->rowCount() != 0) {
            echo "ERROR: The default addresses are already created\n";
            exit(0);
        }

        //Lookup for the admin address
        $query = "SELECT email,domain_id FROM admin ".
            "INNER JOIN domain ON admin.admin_id = domain.domain_admin_id " .
            "WHERE domain.domain_name = '" . $dom_parent . "' ";
         $result = $pdo->query($query);
        if ($result->rowCount() == 0) {
            echo "ERROR: There was a problem, the domain should have admin email defined\n";
            exit(0);
        }
        $sub_id = 0; //TODO defaulting it to 0, could have a different value when used to create mail for subdomains
        $row = $result->fetch(PDO::FETCH_ASSOC);
        $user_email = $row['email'];
        $domain_id=$row['domain_id'];
        //Some variables need to be stored on the session so we can use imscp functions
        $_SESSION['user_id'] = $user_id;
        $_SESSION['user_email'] = $user_email;

        client_mail_add_default_accounts($pdo, $domain_id, $user_email, $domain, $alias, $sub_id);

    break;
    case "create_dom_alias":
        //Requires $domain, $dom_dst
        info("Creating an alias of $dom_dst named $domain");

        //Some code taken from gui/reseller/alias_order.php

        //Destination domain verification (it MUST exist)
        $query = "SELECT domain_id FROM domain WHERE domain_name='" . $dom_dst . "'" ;
        if ($result = $pdo->query($query)) {
            if ($result->rowCount() == 0) {
                echo "ERROR: You MUST have the destination domain ". $dom_dst ." before doing an alias\n";
                exit(0);
            }
        } else {
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

        $row=$result->fetch(PDO::FETCH_ASSOC);
        $domain_id=$row['domain_id'];

        //Domain alias verification
        $result = $pdo->query("SELECT `alias_name` FROM `domain_aliasses` WHERE `alias_name`='$domain'");
        if ($result->rowCount()>0) {
            echo "ERROR: You are trying to create an already existant domain alias\n";
            exit(0);
        }

        //To create webmaster, postmaster, abuse use client_mail_add_default_accounts
        //TODO, check for the case where the alias is only pending activation.
        //Insert the alias of the domain.
        //	$result = $pdo->query("SELECT `alias_name` FROM `domain_aliasses` WHERE `alias_name`='$domain'");
        $query = "INSERT INTO `domain_aliasses` (`domain_id`, `alias_name`, `alias_mount`, `alias_status`, `alias_ip_id`, `url_forward`) ".
             "VALUES ('".$domain_id."','".$domain."','/','toadd','1','http://www.".$dom_dst."/')";
        if (!$pdo->query($query)) {
            echo "There was an error while creating the domain alias\n";
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }
        $cmd = "/var/www/imscp/engine/imscp-rqst-mngr";
        exec ($cmd,$output,$return_var);
        if ($return_var!=0){
            error("rqst-mngr experienced issues dumping output");
            debug($output);
        } else {
            info("rqst-mngr ok, no problems detected");
        }

        //The default mailboxes are created with the create_default_mail

    break;
    case "create_subdomain":
        //Requires $domain, $subdomain
        info("Creating the subdomain $subdomain for $domain");

        //Destination domain verification (it MUST exist)
        $query = "SELECT domain_id FROM domain WHERE domain_name='" . $domain . "'" ;
        if ($result = $pdo->query($query)) {
            if ($result->rowCount() == 0) {
                echo "You MUST have de destination domain ". $domain ." before creating a subdomain\n";
                exit(0);
            }
        } else {
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }
        $row=$result->fetch(PDO::FETCH_ASSOC);
        $domain_id=$row['domain_id'];

        //Check if the subdomain exists
        $query = "SELECT subdomain_name FROM subdomain WHERE subdomain_name = '" . $subdomain . "'" ;
        if ($result = $pdo->query($query)) {
            if ($result->rowCount()>0) {
                echo "ERROR: You are trying to create an already existant subdomain\n";
                exit(0);
            }
        } else {
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

        //Inserting the subdomain into the database
        $query = "INSERT INTO `subdomain`(`domain_id`, `subdomain_name`, " .
            "`subdomain_mount`, `subdomain_url_forward`, `subdomain_status`) " .
            "VALUES ('" . $domain_id. "', '" . $subdomain . "','/" . $subdomain ."', 'no', 'toadd')";
        if ($pdo->query($query)) {
            info("Insert of subdomain successful");
        } else {
            error("There was an error while creating the subdomain $subdomain");
            exit(0);
        }

        //TODO, in case it's needed, create the mountpoint

    break;
    case "create_ftp":
        //Requires $domain, $password
        //Optional $user
        //TODO At the moment it only creates the main FTP user (only for the main dmn)
        //	it could be improved to work also with 'als' and 'sub'
        //TODO, it doesn't do custom mountpoints, this could be parametrized
        //Domain verification
        info("Creating the default ftp for $domain");
        $query = "SELECT `domain_admin_id`,`admin_sys_uid`,`admin_sys_gid` FROM domain INNER JOIN admin ON domain_admin_id=admin_id WHERE domain_name = '".$domain."'";
        $result = $pdo->query($query);
        if ($result->rowCount() == 0) {
            echo "ERROR: You are trying to use a non existant domain\n";
            exit(0);
        } else {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $admin_id = $row['domain_admin_id'];
            $ftp_gid = $row['admin_sys_uid'];
            $ftp_uid = $row['admin_sys_gid'];
        }

        if (empty($user)) {
          $_POST['username'] = str_replace(".", "", $domain);
        } else {
          $_POST['username'] = str_replace(".", "", $user);
        }



        //Ftp user verification
        $query = "SELECT userid FROM ftp_users WHERE userid = '" .
            $user . "@" . $domain . "'";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            echo "ERROR: You are trying to create an already existant FTP user\n";
            exit(0);
        }

        //TODO, this variables have to read from the imscp.conf file
        $ftp_user = $user . $cfg->FTP_USERNAME_SEPARATOR . $domain;
        $ftp_home = $cfg->FTP_HOMEDIR . "/$domain";

        $ftp_shell = $cfg->CMD_SHELL;
        $ftp_passwd = crypt_user_pass_with_salt($password);

        $query = "
            INSERT INTO ftp_users
                (`userid`, `admin_id`, `passwd`, `uid`, `gid`, `shell`, `homedir`)
            VALUES
                (?, ?, ?, ?, ?, ?, ?)
        ";

        $stmt=$pdo->prepare($query);
        if(!$stmt->execute(array($ftp_user, $admin_id, $ftp_passwd, $ftp_uid, $ftp_gid, $ftp_shell, $ftp_home))){
            echo "ERROR: FTP User could not be inserted\n";
        }
        write_log($pdo, $domain . ": add new FTP account: $ftp_user");

    break;
    case "create_mail":
        //Requires $domain, $user, $password
        //Optional $mail_dst if it has also a redirection
        info("Creating the mailbox $user@$domain");

        //Domain verification
        $result = $pdo->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
        if ($result->rowCount()>0) {
          $row=$result->fetch(PDO::FETCH_ASSOC);
          $domain_id=$row['domain_id'];
          } else {
              echo "ERROR: You are trying to use a non-existant domain\n";
              exit(0);
        }

        //Email verification
        $result = $pdo->query("SELECT `mail_addr` FROM `mail_users` WHERE `mail_addr`='$user@$domain'");
        if ($result->rowCount() !=0) {
            error("The mail address $user@$domain already exists");
            exit(0);
        }

        if ($mail_dst != '') {
            $mail_type = "normal_mail,normal_forward";
            $mail_forw = $mail_dst;
        } else {
            $mail_type = "normal_mail";
            $mail_forw = "_no_";
        }

        //TODO, allow the quota to be set with a parameter
        $sql = "INSERT INTO `mail_users` VALUES ('','$user','$password','" . $mail_forw .
            "','$domain_id','" . $mail_type ."',0,'toadd',0,'',0,'$user@$domain')";
        //echo $sql . "\n";

        //Adding the user
        if (!$pdo->query($sql)) {
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

    break;
    case "create_mail_alias":
        //Requires $domain, $user, $mail_dst,
        info("Creating the mailbox alias $user@$domain pointing to $mail_dst");

        //Domain verification
        $result = $pdo->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
        if ($result->rowCount() > 0) {
                  $row=$result->fetch(PDO::FETCH_ASSOC);
                  $domain_id=$row['domain_id'];
        } else {
              echo "ERROR: You are trying to use a non-existant domain\n";
              exit(0);
        }

        //Email verification
        $result = $pdo->query("SELECT `mail_addr` FROM `mail_users` WHERE `mail_addr`='$user@$domain'");
        if ($result->rowCount() !=0) {
            error("The mail address $user@$domain already exists");
            exit(0);
        }

        //TODO, allow the quota to be set with a parameter
        //Adding the alias
        $query = "INSERT INTO `mail_users` VALUES  ('','".$user."','_no_" .
            "','" . $mail_dst . "','" . $domain_id . "','normal_forward',0,'toadd',0,'',0,'" .
             $user . "@" . $domain . "')";

        //Adding the user
        if (!$pdo->query($query)) {
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

    break;
    case "fix_htdocs_owner":
        //Requires $domain
        //Fix the htdocs owner so it's the same as in this computer
        //	useful when migrating content from another machine via rsync
        //Domain verification
        $result = $pdo->query("SELECT `domain_admin_id` FROM `domain` WHERE `domain_name`='$domain'");
        if ($result->rowCount() == 0) {
            echo "ERROR: You cannot change permisions on a non-existant domain\n";
            exit(0);
        } else {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $domain_admin_id=$row['domain_admin_id'];
        }

        //Obtain uid and gid
        $query="SELECT `admin_sys_uid`,`admin_sys_gid` FROM `admin` WHERE `admin_id`='$domain_admin_id'";
        if ($result = $pdo->query($query)) {;
            if ($result->rowCount() > 0) {
                $row=$result->fetch(PDO::FETCH_ASSOC);
                $uid=$row['admin_sys_uid'];
                $gid=$row['admin_sys_gid'];
                $cmd =  "chown -R ".$uid .":".$gid." /var/www/virtual/".$domain."/htdocs/";
                exec ($cmd,$output);
                $cmd =  "chmod 775 /var/www/virtual/" . $domain . "/htdocs";
                exec ($cmd,$output,$return);
                $cmd =  "chown ".$uid .":".$gid." /var/www/virtual/".$domain;
                exec ($cmd,$output);
                $cmd =  "chmod 750 /var/www/virtual/" . $domain;
                info("Fixing the permissions of the web directory for $domain...");
                //It might be necessary to change the permissions in /statistics too
                exec ($cmd,$output,$return);
                if($return!=0) {
                    error("ERROR: Could not fix the permssions for domain $domain");
                }
            } else {
                echo "ERROR: There was an error obtaining the user uid and gid\n";
                exit(0);
            }
        } else {
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

    break;
    case "create_db":
        //Requires $domain, $db_name
        info("Creating the database $db_name in the domain $domain");
        //Domain verification
        $result = $pdo->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $domain_id=$row['domain_id'];
        } else {
            echo "ERROR: You are trying to use a non-existant domain\n";
            exit(0);
        }

        //Database verification
        $query = "SELECT `sqld_name` FROM `sql_database` WHERE `sqld_name`='$db_name'";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            echo "WARN: You are trying create an already existant database\n";
            info("Reloading mysql anyway (in case you resynced and existant database via rsync");
            $cmd = "/etc/init.d/mysql reload\n";
            exec ($cmd,$output,$return);
            if($return!=0) {
                echo "ERROR: Could not reload mysql\n";
            }
            exit(0);
        }

        //Query to insert the database to the control panel
        $query = "INSERT INTO sql_database (domain_id, sqld_name) " .
            "VALUES ('" . $domain_id . "', '" . $db_name . "')";
        if ($pdo->query($query)) {
            echo "Creation of the database in imscp successful \n";
            echo "Reloading the database server (in case you moved via rsync the data)\n";
            $cmd = "/etc/init.d/mysql reload";
            exec ($cmd,$output,$return);
            if($return!=0) {
                echo "ERROR: Could not reload mysql";
            }
        } else {
            echo "There was an error while creating the database in imscp\n";
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

    break;
    case "create_db_user":
        //Requires $domain, $db_name, $user, $password
        info("Creating the database user $user for the database $db_name in the domain $domain");
        //Domain verification
        $query = "SELECT sqld_id FROM sql_database " .
             "INNER JOIN domain ON domain.domain_id = sql_database.domain_id " .
            "WHERE `domain_name`='" . $domain . "' AND sqld_name = '" . $db_name ."'";
        if ($result = $pdo->query($query)) {
            if ($result->rowCount() > 0) {
                $row=$result->fetch(PDO::FETCH_ASSOC);
                $sqld_id=$row['sqld_id'];
            } else {
                error("You are trying to use a non-existant domain or the database $db_name ".
                    "has not been created");
                exit(0);
            }
        } else {
            echo "There was an error while creating the database in imscp\n";
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

        //Database verification
        $query = "SELECT * FROM sql_user ".
            "INNER JOIN sql_database ON sql_database.sqld_id = sql_user.sqld_id ".
            "WHERE sqlu_name = '".$user."' ".
            "AND sqld_name = '".$db_name."';";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            echo "ERROR: You are trying create an already existant database user\n";
            exit(0);
        }

        //Query to insert the db_user to the control panel
        $query = "INSERT INTO sql_user (sqld_id, sqlu_name, sqlu_pass) " .
            "VALUES ('" . $sqld_id . "', '" . $user . "', '" . $password . "')";
        if ($pdo->query($query)) {
            echo "Creation of the database user in imscp successful \n";
        } else {
            echo "There was an error while creating the database user in imscp\n";
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

        //Grant the permissions of the database to the user
        $query = "USE mysql; " .
            "GRANT ALL PRIVILEGES ON ". $db_name .".* TO '". $user ."'@'%' " .
             "IDENTIFIED BY '" . $password . "'; " .
            "GRANT ALL PRIVILEGES ON ". $db_name .".* TO '". $user ."'@localhost " .
             "IDENTIFIED BY '" . $password . "'; " .
            "FLUSH PRIVILEGES;";
        //echo $query . "\n";
        //TODO use something such as pdo_more_results to check for errors
        if ($pdo->query($query)) {
            echo "The user rights where succesfully granted \n";
        } else {
            echo "There was an error while granting the user righs for the database\n";
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }
    break;
    case "create_htaccess_user":
        //Requires $domain $user $password
        //The recomendation would be to the same value of $domain as $user

        //Domain verification
        $result = $pdo->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $domain_id=$row['domain_id'];
        } else {
            echo "ERROR: The domain doesn't exist\n";
            exit(0);
        }

        //user verification
        $query = "SELECT `id` FROM `htaccess_users` WHERE dmn_id='".$domain_id."' ".
          "AND uname='".$user."'";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            echo "ERROR: You cannot create an already existant user\n";
            exit(0);
        }

        $password_hash=crypt_user_pass_with_salt($password);
        $query = "INSERT INTO htaccess_users (`dmn_id`, `uname`, `upass`, `status`) ".
          "VALUES ('".$domain_id."', '".$user."', '".$password_hash."', 'toadd');";
        if (!($pdo->query($query))) { echo "sql_error"; exit(0); };

    break;
    case "join_htaccess_group":
        //Requires $domain, $user
        //Optional $group (default is statistics)

        if(empty($group)){
            $group='statistics';
        }
        //Domain verification
        $result = $pdo->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $domain_id=$row['domain_id'];
        } else {
                echo "ERROR: The domain doesn't exist\n";
                exit(0);
        }

        $query = "SELECT `id` FROM `htaccess_users` WHERE dmn_id='".$domain_id."' ".
          "AND uname='".$user."'";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $user_id=$row['id'];
        } else {
            echo "ERROR: You cannot assing an unexistant user\n";
            exit(0);
        }

        $query = "SELECT id,members FROM htaccess_groups WHERE dmn_id='".$domain_id."' ".
            "AND ugroup='".$group."'";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $group_id=$row['id'];
            //TODO Whe should check if $user_id is already part of the group (using explode)
            if ($row['members'] == '') {
              $members = $user_id;
            } else {
                if(!in_array($user_id,explode(',',$row['members']))) {
                    $members=$row['members'].",".$user_id;
                } else {
                    error("The user ".$user." is already in the group ".$group);
                    exit(0);
                }
            }
            $query = "UPDATE htaccess_groups SET members='".$members."' WHERE id='".$group_id."';";
        } else {
            $members=$user_id;
            $query = "INSERT INTO `htaccess_groups` (`dmn_id`, `ugroup`, `members`, `status`) ".
              "VALUES ('".$domain_id."', '".$group."', '".$members."', 'toadd');";
        }
        if (!($pdo->query($query))) { echo "sql_error"; exit(0); };

    break;
    case "change_domain_pass":
        //Requires $domain, $password
        //Domain verification
        $query="SELECT `domain_admin_id` FROM `domain` WHERE `domain_name`='".$domain."';";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $admin_id=$row['domain_admin_id'];
     } else {
                echo "ERROR: You cannot change the password for a non-existant domain\n";
                exit(0);
        }
        $password_hash=crypt_user_pass_with_salt($password);
        $query = "UPDATE `admin` SET `admin_pass` = '".$password_hash."' ".
            "WHERE `admin_id` = '".$admin_id."';";
        if (!($pdo->query($query))) { echo "sql_error"; exit(0); };
    break;
    case "change_mailuser_pass":
        //Requires $mail, $password
        echo "Updating password of mailbox $mail\n";

        //Email verification
        $result = $pdo->query("SELECT `mail_addr` FROM `mail_users` WHERE `mail_addr`='$mail'");
        if ($result->rowCount() != 1) {
            echo "ERROR: You are trying to update a non existant mail address\n";
            exit(0);
        }

        $enc_pass =  encrypt_db_password($password);
        $sql = "UPDATE `mail_users` SET `mail_pass`= '".$enc_pass."' WHERE `mail_addr`='".$mail."';";
        //echo $sql . "\n";

        //Updating the user
        if (!$pdo->query($sql)) {
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }

    break;
    case "set_mail_limit":
        //Requires $domain, $limit
        $query="SELECT `domain_id` FROM `domain` WHERE `domain_name`='".$domain."';";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $domain_id=$row['domain_id'];
        } else {
            echo "ERROR: You cannot disable for a non-existant domain\n";
            exit(0);
        }

        $query="UPDATE `domain` SET domain_mailacc_limit='".$limit."' WHERE domain_id='".
            $domain_id."';";
        if (!($pdo->query($query))) { echo "sql_error"; exit(0); };

    break;
    case "regenerate_domain":
        //Requires $domain
        //Domain verification
        $result = $pdo->query("SELECT `domain_id` FROM `domain` WHERE `domain_name`='$domain'");
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $domain_id=$row['domain_id'];
        } else {
            echo "ERROR: You cannot regenerate a non-existant domain\n";
            exit(0);
        }

        //Change to 'tochange' the status on the database
        $query = "USE ".$imscpdb_name."; " .
            "UPDATE `domain` SET `domain_status` = 'tochange' " .
            "WHERE `domain_status` = 'ok' AND domain_id = ".$domain_id."; " .
            "UPDATE `subdomain` SET `subdomain_status` = 'tochange' " .
            "WHERE `subdomain_status` = 'ok' AND domain_id = ".$domain_id."; " .
            "UPDATE `domain_aliasses` SET `alias_status` = 'tochange' ".
            "WHERE `alias_status` = 'ok' AND domain_id = ".$domain_id."; " .
            "UPDATE `mail_users` SET `status` = 'tochange' ".
            "WHERE `status` = 'ok' AND domain_id = ".$domain_id.";";

        //TODO use pdo_more_results to check for errors
        if ($pdo->query($query)) {
            echo "#The domain ".$domain." is scheduled for regeneration\n";
        } else {
            echo "ERROR: There was an error while scheduling the regeneration of ".$domain."\n";
            printf("ERROR: %s\n", $pdo->error);
            exit(0);
        }
        echo "#To launch the regeneration of $domain from console, you should do:\n";
        echo "/var/www/imscp/engine/imscp-rqst-mngr\n";
    break;
    case "delete_alias":
        //With this action, we will delete an alias from a domain_admin_id
        //Requires $domain, $dom_dst
        //Domain verification
        $query = "SELECT domain_id FROM `domain` ".
          "WHERE `domain_name`='".$dom_dst."';";
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $domain_id=$row['domain_id'];
        } else {
            error("ERROR: You cannot delete an alias from a non-existant domain");
            exit(0);
        }

        //Alias verification
        $query = "SELECT * FROM domain_aliasses ".
          "WHERE alias_name='".$domain."' AND domain_id = '".$domain_id."'";
        $result = $pdo->query($query);
        if ($result->rowCount() != 1) {
            echo "ERROR: You cannot delete a non-existant alias\n";
            exit(0);
        }

        $query = "UPDATE `domain_aliasses` SET `alias_status` = 'todelete' ".
          "WHERE alias_name = '".$domain."' AND domain_id = '".$domain_id."';";
         if (!($pdo->query($query))) { echo "sql_error"; exit(0); };
    break;
    case "delete_domain":
        //With this action, we will delete all the data from the user
        //	mailboxes, web, etc ...
        //Requires $domain
        //Domain verification
        $query = "SELECT domain_id,admin_sys_uid,admin_sys_gid,domain_admin_id,admin_name ".
            " FROM `domain` ".
            " JOIN `admin`  ON admin_id= domain_admin_id".
            " WHERE `domain_name`='".$domain."'";
        info($query);
        $result = $pdo->query($query);
        if ($result->rowCount() > 0) {
            $row=$result->fetch(PDO::FETCH_ASSOC);
            $domain_id=$row['domain_id'];
            $admin_sys_uid=$row['admin_sys_uid'];
            $admin_sys_gid=$row['admin_sys_gid'];
            $domain_admin_id=$row['domain_admin_id'];
            $domain_admin_username=$row['admin_name'];
        } else {
            echo "ERROR: You cannot delete a non-existant domain\n";
            exit(0);
        }

        try {
            //By using transactions, we will be able to rollBack
            $pdo->beginTransaction();

            // Delete the databases
            $query = "SELECT `sqld_id`,`sqld_name` FROM `sql_database` WHERE `domain_id` = '".$domain_id."';";
            echo $query."\n";
            foreach ($pdo->query($query) as $row) {
                $sqld_id=$row['sqld_id'];
                delete_sql_database($pdo, $domain_id, $sqld_id);
            }

            //Finally, commit all the changes
            $pdo->commit();
        } catch (Exception $e) {
          $pdo->rollBack();
          echo "ERROR: Rolling back database updates: " . $e->getMessage()."\n";
        }

        try {
            //By using transactions, we will be able to rollBack
            $pdo->beginTransaction();

            //Query to schdedule for deletion the domain
            $query = "DELETE FROM `login` WHERE `user_name` = '".$domain_admin_username."'";
            echo $query."\n";
            $pdo->query($query);

            //Query to schdedule for deletion the domain
            $query = "UPDATE `mail_users` SET `status` = '".$cfg->ITEM_TODELETE_STATUS."' ".
              "WHERE `domain_id` = '".$domain_id."';";
            echo $query."\n";
            $pdo->query($query);

            //Schedule deletion of domain_aliasses
            $query = "UPDATE `domain_aliasses` SET `alias_status` = '".$cfg->ITEM_TODELETE_STATUS."' ".
              "WHERE `domain_id` = '".$domain_id."';";
            echo $query."\n";
            $pdo->query($query);
            //Schedule deletion of subdomains
            $query = "UPDATE `subdomain` SET `subdomain_status` = '".$cfg->ITEM_TODELETE_STATUS."' ".
              "WHERE `domain_id` = '".$domain_id."';";
            $pdo->query($query);
            echo $query."\n";
            // Schedule deletion of SSL certificates
            $query = "UPDATE `ssl_certs` SET `status` = '".$cfg->ITEM_TODELETE_STATUS."' WHERE `type` = 'dmn' AND `id` = '".$cfg->ITEM_TODELETE_STATUS."'";
            echo $query . "\n";
            $pdo->query($query);
            // Schedule deletion of SSL certificates
            $query = "UPDATE `ssl_certs` SET `status` = '".$cfg->ITEM_TODELETE_STATUS."' WHERE `type` = 'als' AND `id` IN (SELECT `alias_id` FROM `domain_aliasses` WHERE `domain_id` = '".$cfg->ITEM_TODELETE_STATUS."')";
            echo $query . "\n";
            $pdo->query($query);
            // Schedule deletion of SSL certificates
            $query = "UPDATE `ssl_certs` SET `status` = '".$cfg->ITEM_TODELETE_STATUS."' WHERE `type` = 'sub' AND `id` IN (SELECT `subdomain_id` FROM `subdomain` WHERE `domain_id` = '".$cfg->ITEM_TODELETE_STATUS."')";
            echo $query . "\n";
            $pdo->query($query);

            //Finally we mark the domain for removal
            $query = "UPDATE `domain` SET `domain_status` = '".$cfg->ITEM_TODELETE_STATUS."' ".
              "WHERE `domain_id` = '".$domain_id."';";
            echo $query . "\n";
            $pdo->query($query);

        } catch (Exception $e) {
            $pdo->rollBack();
            echo "ERROR: Rolling back database updates: " . $e->getMessage()."\n";
        }

        //At this point the deletion is scheduled, rqst-mngr has to be run now to
        // be successful
        $cmd = "/var/www/imscp/engine/imscp-rqst-mngr";
        exec ($cmd,$output,$return_var);
        if ($return_var!=0){
            error("rqst-mngr experienced issues dumping output");
            debug($output);
        } else {
            info("rqst-mngr ok, no problems detected");
        }

        try {
            //Deleting the htaccess entries
            $query = "
            DELETE
                    `areas`, `users`, `groups`
            FROM
                    `domain` `dmn`
            LEFT JOIN
                    `htaccess` `areas` ON (`areas`.`dmn_id` = `dmn`.`domain_id`)
            LEFT JOIN
                    `htaccess_users` `users` ON (`users`.`dmn_id` = `dmn`.`domain_id`)
            LEFT JOIN
                    `htaccess_groups` `groups` ON (`groups`.`dmn_id` = `dmn`.`domain_id`)
            WHERE
                    `dmn`.`domain_id` = '".$domain_id."'
            ";
            echo $query . "\n";
            $pdo->query($query);


            //Deleting traffic entriess
            $query = "DELETE FROM `domain_traffic` WHERE `domain_id` = '".$domain_id."'";
            echo $query . "\n";
            $pdo->query($query);

            //Deleting dns entries
            $query = "DELETE FROM `domain_dns` WHERE `domain_id` = '".$domain_id."'";
            echo $query . "\n";
            $pdo->query($query);

            //Deleting ftp user
            $query = "DELETE FROM `ftp_users` WHERE `uid` = '".$admin_sys_uid."'";
            echo $query . "\n";
            $pdo->query($query);

            //Deleting ftp group
            $query = "DELETE FROM `ftp_group` WHERE `gid` = '".$admin_sys_gid."'";
            echo $query . "\n";
            $pdo->query($query);

            //Deleting admin user
            $query = "DELETE FROM `admin` WHERE `admin_id` = '".$domain_admin_id."'";
            echo $query . "\n";
            $pdo->query($query);

            //Deleting the quota limits
            $query = "DELETE FROM `quotalimits` WHERE `name` = '".$domain."'";
            echo $query . "\n";
            $pdo->query($query);

            //Deleting the quota tallies
            $query = "DELETE FROM `quotatallies` WHERE `name` = '".$domain."'";
            echo $query . "\n";
            $pdo->query($query);

            //Deleting the support tickets
            $query = "DELETE FROM `tickets` WHERE ticket_from = '".$domain_admin_id."' OR ticket_to = '".$domain_admin_id."'";
            echo $query . "\n";
            $pdo->query($query);

            //Deleting user gui properties
            $query = "DELETE FROM `user_gui_props` WHERE `user_id` = '".$domain_admin_id."'";
            echo $query . "\n";
            $pdo->query($query);

            //Deleting php ini entries
            $query = "DELETE FROM `php_ini` WHERE `domain_id` = '".$domain."'";
            echo $query . "\n";
            $pdo->query($query);

            //Finally, commit all the changes
            $pdo->commit();
        } catch (Exception $e) {
          $pdo->rollBack();
          echo "ERROR: Rolling back database updates: " . $e->getMessage();
        }
    break;
    default:
        echo "ERROR: You have selected a non existant action\n";
        exit(0);
    break;
}

//If we could reach this point, then we tell the engine to complete
//	the process
send_request();

?>
