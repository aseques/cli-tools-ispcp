<?php

//This file includes some functions taken out of domain.php to 
//	clarify and modularize the code


function get_ftp_user_gid(&$sql, $dmn_name, $admin_id, $ftp_user) {

	global $last_gid, $max_gid;

	$query = "SELECT `gid`, `members` FROM `ftp_group` WHERE `groupname` = ?";
	$rs = exec_query($sql, $query, $dmn_name);

	if ($rs->recordCount() == 0) { // there is no such group. we'll need a new one.
		list($temp_dmn_id,
			$temp_dmn_name,
			$temp_dmn_gid,
			$temp_dmn_uid,
			$temp_dmn_created_id,
			$temp_dmn_created,
			$temp_dmn_expires,
			$temp_dmn_last_modified,
			$temp_dmn_mailacc_limit,
			$temp_dmn_ftpacc_limit,
			$temp_dmn_traff_limit,
			$temp_dmn_sqld_limit,
			$temp_dmn_sqlu_limit,
			$temp_dmn_status,
			$temp_dmn_als_limit,
			$temp_dmn_subd_limit,
			$temp_dmn_ip_id,
			$temp_dmn_disk_limit,
			$temp_dmn_disk_usage,
			$temp_dmn_php,
			$temp_dmn_cgi,
			$allowbackup,
			$dmn_dns
		) = get_domain_default_props($sql, $admin_id);

		$query = "
			INSERT INTO ftp_group
				(`groupname`, `gid`, `members`)
			VALUES
				(?, ?, ?)
		";

		$rs = exec_query($sql, $query, array($dmn_name, $temp_dmn_gid, $ftp_user));
		// add entries in the quota tables
		// first check if we have it by one or other reason
		$query = "SELECT COUNT(`name`) AS cnt FROM `quotalimits` WHERE `name` = ?";
		$rs = exec_query($sql, $query, $temp_dmn_name);
		if ($rs->fields['cnt'] == 0) {
			// ok insert it
			if ($temp_dmn_disk_limit == 0) {
				$dlim = 0;
			} else {
				$dlim = $temp_dmn_disk_limit * 1024 * 1024;
			}

			$query = "
				INSERT INTO `quotalimits`
					(`name`, `quota_type`, `per_session`, `limit_type`,
					`bytes_in_avail`, `bytes_out_avail`, `bytes_xfer_avail`,
					`files_in_avail`, `files_out_avail`, `files_xfer_avail`)
				VALUES
					(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
			";

			$rs = exec_query($sql, $query, array($temp_dmn_name, 'group', 'false', 'hard', $dlim, 0, 0, 0, 0, 0));
		}

		return $temp_dmn_gid;
	} else {
		$ftp_gid = $rs->fields['gid'];
		$members = $rs->fields['members'];

		if (preg_match("/" . $ftp_user . "/", $members) == 0) {
			$members .= ",$ftp_user";
		}

		$query = "
			UPDATE
				`ftp_group`
			SET
				`members` = ?
			WHERE
				`gid` = ?
			AND
				`groupname` = ?
		";

		$rs = exec_query($sql, $query, array($members, $ftp_gid, $dmn_name));
		return $ftp_gid;
	}
}

function get_ftp_user_uid(&$sql, $dmn_name, $admin_id, $ftp_user, $ftp_user_gid) {

	global $max_uid;

	$query = "
		SELECT
			`uid`
		FROM
			`ftp_users`
		WHERE
			`userid` = ?
		AND
			`gid` = ?
	";

	$rs = exec_query($sql, $query, array($ftp_user, $ftp_user_gid));
	if ($rs->recordCount() > 0) {
		set_page_message(tr('FTP account already exists!'));
		return -1;
	}

	list($temp_dmn_id,
		$temp_dmn_name,
		$temp_dmn_gid,
		$temp_dmn_uid,
		$temp_dmn_created_id,
		$temp_dmn_created,
		$temp_dmn_expires,
		$temp_dmn_last_modified,
		$temp_dmn_mailacc_limit,
		$temp_dmn_ftpacc_limit,
		$temp_dmn_traff_limit,
		$temp_dmn_sqld_limit,
		$temp_dmn_sqlu_limit,
		$temp_dmn_status,
		$temp_dmn_als_limit,
		$temp_dmn_subd_limit,
		$temp_dmn_ip_id,
		$temp_dmn_disk_limit,
		$temp_dmn_disk_usage,
		$temp_dmn_php,
		$temp_dmn_cgi,
		$allowbackup,
		$dmn_dns
	) = get_domain_default_props($sql, $admin_id);

	return $temp_dmn_uid;
}



/**
 * Reads line from the socket resource.
 *
 * @param resource &$socket
 * @return string A line read from the socket resource
 */

if (!function_exists('read_line')) { 

	function read_line(&$socket)
	{
        	$line = '';

	        do {
        	        $ch = socket_read($socket, 1);
                	$line = $line . $ch;
	        } while ($ch != "\r" && $ch != "\n");

        	return $line;
	}
}

/**
 * Send a request to the daemon.
 *
 * @return string Daemon answer
 * @todo Remove error operator
 */
if (!function_exists('send_request')) {
function send_request()
{
	/** @var $cfg  iMSCP_Config_Handler_File */
	$cfg = iMSCP_Registry::get('config');

	//$code = 999;

	@$socket = socket_create(AF_INET, SOCK_STREAM, 0);
	if ($socket < 0) {
		$errno = "socket_create() failed.\n";
		return $errno;
	}

	@$result = socket_connect($socket, '127.0.0.1', 9876);
	if ($result == false) {
		$errno = "socket_connect() failed.\n";
		return $errno;
	}

	// read one line with welcome string
	$out = read_line($socket);

	list($code) = explode(' ', $out);
	if ($code == 999) {
		return $out;
	}

	// send hello query
	$query = "helo  {$cfg->Version}\r\n";
	socket_write($socket, $query, strlen($query));

	// read one line with helo answer
	$out = read_line($socket);

	list($code) = explode(' ', $out);
	if ($code == 999) {
		return $out;
	}

	// send reg check query
	$query = "execute query\r\n";
	socket_write($socket, $query, strlen($query));
	// read one line key replay
	$execute_reply = read_line($socket);

	list($code) = explode(' ', $execute_reply);
	if ($code == 999) {
		return $out;
	}

	// send quit query
	$quit_query = "bye\r\n";
	socket_write($socket, $quit_query, strlen($quit_query));

	// read quit answer
	$quit_reply = read_line($socket);

	list($code) = explode(' ', $quit_reply);

	if ($code == 999) {
		return $out;
	}

	list($answer) = explode(' ', $execute_reply);

	socket_close($socket);

	return $answer;
}
}
